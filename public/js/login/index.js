$(function(){  		
 		 $('#form').validate({
 		        rules: {
 		        'password': 'required',	        
 		        'userName': 'required'
 		        },
 		    messages: {
 		        'password': 'Campo obligatorio',
 		        'userName': 'Campo obligatorio'
 		    },
 		   
 		    submitHandler: function(form){
 		    	form.submit(); 		    
 		    } 
 		 });
  		
  	});