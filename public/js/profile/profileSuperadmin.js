$(function () {
	
	$.validator.addMethod( //override email with django email validator regex - fringe cases: "user@admin.state.in..us" or "name@website.a"
	        'email',
	        function(value, element){
	            return this.optional(element) || /(^[-!#$%&'*+/=?^_`{}|~0-9A-Z]+(\.[-!#$%&'*+/=?^_`{}|~0-9A-Z]+)*|^"([\001-\010\013\014\016-\037!#-\[\]-\177]|\\[\001-\011\013\014\016-\177])*")@((?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)$)|\[(25[0-5]|2[0-4]\d|[0-1]?\d?\d)(\.(25[0-5]|2[0-4]\d|[0-1]?\d?\d)){3}\]$/i.test(value);
	        },
	        'Verify you have a valid email address.'
	    );
	
	$("#form").validate({
		rules:{
			username:{
				required:true
			},
			password:{
				required:true
			},
			confirmPassword:{
				required:true,
				equalTo: "#password"
			}
		},
		messages:{
			username:{
				required: " *Este campo es requerido"
			},password:{
				required: " *Este campo es requerido"
			},confirmPassword:{
				required: " *Este campo es requerido",
				equalTo: "*Las contrase&ntilde;as no coinciden"
			}
		},
        
        submitHandler: function(form) {
				addErrorUsername();
				validateUsername();
        }
	});
    
    
    function validateUsername(){
		$('input[type=submit]').prop('disabled',true);  
		var url = $("#urlValidateUsername").val();
		var username = $("#username").val();
        var idUser = $("#idUser").val();
		$.ajax({
    		type:"POST",
    		url:url,
    		data: {username:username, idUser:idUser},
    		success: function(result){
    			if(result == 1){
    				$('input[type=submit]').prop('disabled',false);  
    				$("#errorUsername").show("slow");
    			}else{
    				updateStablishmentAdmin();	
    			} 
    		}    		
    	});
	}
    

	
	function updateStablishmentAdmin(){
		$('input[type=submit]').prop('disabled',true);  
		var url = $("#urlUpdate").val();
        var idUser = $('#idUser').val();
		var username = $("#username").val();
		var password = $("#password").val();
		$.ajax({
    		type:"POST",
    		url:url,
    		data: {username:username, password:password, idUser:idUser},
    		success: function(result){
                confirmEditProfile();
    			//window.location.href = $("#urlProfile").val();
    		}    		
    	});
	}
    
    function addErrorUsername(){
		$('#showErrorUsername').empty();
		var html = "<div id='errorUsername' style='display: none;' role='alert'>"
				+"<div class='alert alert-warning alert-dismissible'>"+
	            "<button type='button' class='close' data-dismiss='alert'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>"+
	        	"<strong><i class='fa a-exclamation-triangle'></i>&nbsp; El nombre de usuario ya ha sido registrado</strong>"+
	        	"</div>"+
	        "</div>";
		
		$('#showErrorUsername').append(html);
	}
	
    $('.button-checkbox').each(function () {

        // Settings
        var $widget = $(this),
            $button = $widget.find('button'),
            $checkbox = $widget.find('input:checkbox'),
            color = $button.data('color'),
            settings = {
                on: {
                    icon: 'glyphicon glyphicon-check'
                },
                off: {
                    icon: 'glyphicon glyphicon-unchecked'
                }
            };

        // Event Handlers
        $button.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.on('change', function () {
            updateDisplay();
        });

        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');

            // Set the button's state
            $button.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $button.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$button.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $button
                    .removeClass('btn-default')
                    .addClass('btn-' + color + ' active');
            }
            else {
                $button
                    .removeClass('btn-' + color + ' active')
                    .addClass('btn-default');
            }
        }

        // Initialization
        function init() {

            updateDisplay();

            // Inject the icon if applicable
            if ($button.find('.state-icon').length == 0) {
                $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i>');
            }
        }
        init();
    });
});

function activeEditPassword() {
        if($("#edit_pass").is(':checked')){
            $("#password").val("");
            $("#confirmPassword").val("");
            $("#passDiv").hide();
        }else{
            $("#passDiv").show();
        }
}

function confirmEditProfile(){
	$("#dialog-edit").modal('show');
}

function profileRedirect(){
    window.location.href = $("#urlProfile").val();
}
