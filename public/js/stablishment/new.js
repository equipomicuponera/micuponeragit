$(function() {
	
	$('#form').validate({
		rules : {
			'name' : 'required',
			'address' : 'required',
			'description' : 'required',
			'phone' : {required:true,number:true},
			'latitude': 'required',
			'longitude': 'required',
			'stablishment_type' : 'required'
		},
		messages : {
			'name' : 'Debe ingresar un nombre',
			'address' : 'Debe ingresar una direcci&oacute;n',
			'description' : 'Debe ingresar una descripci&oacute;n',
			'phone' : {required:'Debe ingresar un tel&eacute;fono',number:'Debe ingresar un tel&eacute;fono',min:'Ingrese un tel&eacute;fono v&aacute;lido'},
			'latitude': 'Seleccione una ubicaci&oacute;n en el mapa',
			'longitude': 'Requerido',
			'stablishment_type' : 'Seleccione un tipo'
		},
		debug : true,
		/* errorElement: 'div', */
		// errorContainer: $('#errores'),
		submitHandler : function(form) {
			$('input[type=submit]').prop('disabled',true);                 
               // form.submit(); //send empty 
                confirmCreate();
		}
	});
	
	$('input[type="number"]').focusout(function (evt) {
		   if(!$.isNumeric($(this).val())){
		     $(this).val("");
		   }
	});
	
	google.maps.event.addDomListener(window, 'load', initialize);  

	var map;
	var markersArray = [];
	  function initialize() {
		  
	  var myLatlng = new google.maps.LatLng(20.96599639896182,-89.6275455277646);

	  var myOptions = {
	     zoom: 8,
	     center: myLatlng,
	     mapTypeId: google.maps.MapTypeId.ROADMAP
	     }
	  map = new google.maps.Map(document.getElementById("map_canvas"), myOptions); 

//	  var marker = new google.maps.Marker({
//	  draggable: true,
//	  position: myLatlng, 
//	  map: map,
//	  title: "Your location"
//	  });
	  
	  google.maps.event.addListener(map, "click", function(event)
	          {
	              // place a marker
	              placeMarker(event.latLng);

	              // display the lat/lng in your form's lat/lng fields
	              document.getElementById("latitude").value = event.latLng.lat();
	              document.getElementById("longitude").value = event.latLng.lng();
	          });
	 }
	  
	  function placeMarker(location) {
	      // first remove all markers if there are any
	      deleteOverlays();

	      var marker = new google.maps.Marker({
	          position: location, 
	          map: map
	      });

	      // add marker in markers array
	      markersArray.push(marker);

	      //map.setCenter(location);
	  }

	  // Deletes all markers in the array by removing references to them
	  function deleteOverlays() {
	      if (markersArray) {
	          for (i in markersArray) {
	              markersArray[i].setMap(null);
	          }
	      markersArray.length = 0;
	      }
	  }
	
});

function isInArray(value, array) {
	  if (array.indexOf(value) > -1){
		  return true;
	  }else{
		  return false;
	  }
}


function confirmCreate(){
	$("#dialog-create").modal('show');
}
function sendData(){
    form.submit(); //send empty 
}
