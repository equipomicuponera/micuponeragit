$(function(){

	google.maps.event.addDomListener(window, 'load', initialize);  

	var map;
	var markersArray = [];
	var lat = $('#latStablishment').val();
	var long = $('#longStablishment').val();
	  function initialize() {
		  
	  var myLatlng = new google.maps.LatLng(lat,long);

	  var myOptions = {
	     zoom: 8,
	     center: myLatlng,
	     mapTypeId: google.maps.MapTypeId.ROADMAP
	     };
	  map = new google.maps.Map(document.getElementById("map_canvas"), myOptions); 
	  
	  var marker = new google.maps.Marker({
		  position: myLatlng, 
		  map: map,
		  title: "Your location"
		  });
	  markersArray.push(marker);

	  
	  }
	  
	  function placeMarker(location) {
	      // first remove all markers if there are any
	      deleteOverlays();

	      var marker = new google.maps.Marker({
	          position: location, 
	          map: map
	      });

	      // add marker in markers array
	      markersArray.push(marker);

	      //map.setCenter(location);
	  }

	  // Deletes all markers in the array by removing references to them
	  function deleteOverlays() {
	      if (markersArray) {
	          for (i in markersArray) {
	              markersArray[i].setMap(null);
	          }
	      markersArray.length = 0;
	      }
	  }
});



