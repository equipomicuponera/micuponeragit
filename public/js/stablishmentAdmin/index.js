$(function(){
	$('#stablishmentTable').dataTable({
		"sPaginationType": "bs_normal",
		"oLanguage": {
			"sLengthMenu": "Mostrar _MENU_ registros",
			"sZeroRecords": "No existen datos",
			"sInfo": "Registro _START_ al _END_ de _TOTAL_ registros",
			"sInfoEmpty": "Registro 0 al 0 de 0 registros",
			"sInfoFiltered": "(Filtrado de _MAX_ registros)"
		},
		"aoColumnDefs":[
			{ "bSortable": false, "aTargets": [ 4 ] }
		]
	});
	fixDataTable();
	
	
});
function fixDataTable(){
	$('.datatable').each(function(){
		var datatable = $(this);
		// SEARCH - Add the placeholder for Search and Turn this into in-line form control
		var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
		search_input.attr('placeholder', 'Buscar');
		search_input.addClass('form-control input-sm');
		// LENGTH - Inline-Form control
		var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
		length_sel.addClass('form-control input-sm');
        datatable.bind('page', function(e){
            window.console && console.log('pagination event:', e); //this event must be fired whenever you paginate
        });
        datatable.css('width','');
	});
}

var idStablishmentAdmin = null;
function confirmDeleteStablishmentAdmin(id,name){
	idStablishmentAdmin = id;
	$("#nameStablishmentAdmin").text(name);
	$("#dialog-delete").modal('show');
}

function deleteStablishmentAdmin(){
	location.href='stablishment-admin/delete?id='+idStablishmentAdmin;
}