$(function() {
	
	$('#form').validate({
		rules : {
			'name' : 'required',
			'acquired' : {required:true, min: 1},
			'start_date' : 'required',
			'end_date' : 'required',
			'description' : 'required',
			'stablishment' : 'required',
		},
		messages : {
			'name' : 'Debe ingresar un nombre',
			'acquired' : {required:"Debe ingresar el n&uacute;mero de cupones que desea registrar",min:"El m&iacute;nimo de cupones a registrar es 1"},
			'start_date' : 'Debe ingresar una fecha de inicio',
			'end_date' : 'Debe ingresar una fecha de fin',
			'description' : 'Debe ingresar una descripci&oacute;n',
			'stablishment' : 'Debe elegir un establecimiento',
		},
		debug : true,
		/* errorElement: 'div', */
		// errorContainer: $('#errores'),
		submitHandler : function(form) {
			$('input[type=submit]').prop('disabled',true);                 
                confirmCreate();
		}
	});
	
	$('#datePickerStart').datetimepicker({
		minDate: new Date(),
		language: 'es',
		showToday: false,
		pickDate: true,
		pickTime:false,
		format: 'YYYY-MM-DD',
		autoclose: true,
		startView: 3,
		minView: 2
	});

	$('#datePickerStart').data("DateTimePicker").setDate(new Date());

	var date = new Date();
	date.setDate(date.getDate()+1);
	$('#datePickerEnd').datetimepicker({
		minDate: date,
		language: 'es',
		showToday: false,
		pickDate: true,
		pickTime:false,
		format: 'YYYY-MM-DD',
		autoclose: true,
		startView: 3,
		minView: 2
	});

	$('#datePickerEnd').data("DateTimePicker").setDate(date);
		
	$("#datePickerStart").on("dp.change",function (e) {
		var date = new Date(e.date.toDate().getTime());
		date.setDate(date.getDate() + 1);
	    $('#datePickerEnd').data("DateTimePicker").setMinDate(date);
	    
	    var checkout =  $('#datePickerEnd').data("DateTimePicker").getDate();
	    if(e.date.valueOf() >= checkout.valueOf()){
	    	var newDate = new Date(e.date.toDate().getTime());
	    	newDate.setHours(0, 0, 0);
	        newDate.setDate(newDate.getDate() + 1);
	        $('#datePickerEnd').data("DateTimePicker").setDate(newDate);
	    }
	    $('#datePickerEnd')[0].focus();
	 });
});

function isInArray(value, array) {
	  if (array.indexOf(value) > -1){
		  return true;
	  }else{
		  return false;
	  }
}

function confirmCreate(){
	$("#dialog-create").modal('show');
}
function sendData(){
    form.submit(); //send empty 
}