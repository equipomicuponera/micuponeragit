<?php

$app->get('/userType', 'getUserTypes');
$app->get('/userType/:idUserType',	'getUserType');

function getUserTypes() {
	$sqlGetUserTypes = "select * FROM user_type ORDER BY id";
	try {
		$db = getConnection();
		$queryBuilder = $db->query($sqlGetUserTypes);  
		$types = $queryBuilder->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo json_encode($types);
	} catch(PDOException $exception) {
		echo '{"error":'. $exception->getMessage() .'}'; 
	}
}

function getUserType($idUserType) {
	$sqlGetUserType = "SELECT * FROM user_type WHERE id=:id";
	try {
		$db = getConnection();
		$queryBuilder = $db->prepare($sqlGetUserType);  
		$queryBuilder->bindParam("id", $idUserType);
		$queryBuilder->execute();
		$type = $queryBuilder->fetchObject();  
		$db = null;
		echo json_encode($type); 
	} catch(PDOException $exception) {
		echo '{"error":'. $exception->getMessage() .'}'; 
	}
}

?>