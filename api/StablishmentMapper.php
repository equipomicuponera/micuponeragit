<?php
$app->get('/stablishments', 'getAllStablishments');
$app->get('/stablishments/:idStablishmentAdmin', 'getStablishmentsByStablishmentAdmin');
$app->get('/stablishment/:idStablishment',	'getStablishment');
$app->get('/stablishment/byCoupon/:idCoupon',	'getStablishmentByCoupon');
$app->post('/stablishment/:idStablishmentAdmin', 'addStablishment');
$app->put('/stablishment/:idStablishment', 'updateStablishment');
$app->delete('/stablishment/:idStablishment',	'deleteStablishment');

function getAllStablishments() {
	//$sqlGetStablishments = "SELECT * FROM stablishment WHERE status=".ACTIVE."";
	$sqlGetStablishments = 
	"SELECT 
	stablishment.id as id,
	stablishment_type.name as stablishmentTypeName,
	stablishment.name as name,
	address,
	description,
	stablishment.status as status,
	image,
	phone, 
	latitude,
	longitude,
	stablishment_type_id
	FROM stablishment 
	JOIN stablishment_type ON stablishment_type.id = stablishment.stablishment_type_id 
	WHERE stablishment.status=".ACTIVE."";
	
	try {
		$db = getConnection();
		$queryBuilder = $db->query($sqlGetStablishments);
		$stablishments = $queryBuilder->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo json_encode($stablishments);
	} catch(PDOException $exception) {
		echo '{"text":'. $exception->getMessage() .'}';
	}
}

function getStablishmentsByStablishmentAdmin($idStablishmentAdmin) {
	$sqlGetStablishments = 
	"SELECT 
	stablishment.id as id,
	stablishment_type.name as stablishmentTypeName,
	stablishment.name as name,
	address,
	description,
	stablishment.status as status,
	image,
	phone, 
	latitude,
	longitude,
	stablishment_type_id,
	stablishment_admin_id,
	stablishment_id
	FROM stablishment 
	JOIN stablishment_admin_has_stablishment ON stablishment.id = stablishment_admin_has_stablishment.stablishment_id 
	JOIN stablishment_type ON stablishment_type.id = stablishment.stablishment_type_id 
	WHERE stablishment.status=1 AND stablishment_admin_has_stablishment.stablishment_admin_id=".$idStablishmentAdmin;
	try {
		$db = getConnection();
		$queryBuilder = $db->query($sqlGetStablishments);  
		$stablishment = $queryBuilder->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo json_encode($stablishment);
	} catch(PDOException $exception) {
		echo '{"text":'. $exception->getMessage() .'}'; 
	}
}

function getStablishmentByCoupon($idCoupon) {
	$sqlGetStablishments = "SELECT 
	stablishment_has_coupon.stablishment_id as id,
	stablishment.name,
	address,
	description,
	image,
	status,
	stablishment_type.name as stablishmentTypeName,
	latitude,
	longitude,
	phone  
	FROM stablishment 
	JOIN stablishment_has_coupon ON stablishment.id = stablishment_has_coupon.stablishment_id
	JOIN stablishment_type ON stablishment_type.id = stablishment.stablishment_type_id 
	WHERE status=".ACTIVE." AND stablishment_has_coupon.coupon_id=".$idCoupon;
	try {
		$db = getConnection();
		$queryBuilder = $db->query($sqlGetStablishments);
		$stablishment = $queryBuilder->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo json_encode($stablishment);
	} catch(PDOException $exception) {
		echo '{"text":'. $exception->getMessage() .'}';
	}
}

function getStablishment($idStablishment) {
	//$sqlGetStablishment = "SELECT * FROM stablishment WHERE id=:id";
	$sqlGetStablishment = 
	"SELECT 
	stablishment.id as id,
	stablishment_type.name as stablishmentTypeName,
	stablishment.name as name,
	address,
	description,
	stablishment.status as status,
	image,
	phone, 
	latitude,
	longitude,
	stablishment_type_id
	FROM stablishment 
	JOIN stablishment_type ON stablishment_type.id = stablishment.stablishment_type_id 
	WHERE stablishment.id=:id";
	
	try {
		$db = getConnection();
		$queryBuilder = $db->prepare($sqlGetStablishment);  
		$queryBuilder->bindParam("id", $idStablishment);
		$queryBuilder->execute();
		$stablishment = $queryBuilder->fetchObject();  
		$db = null;
		echo json_encode($stablishment); 
	} catch(PDOException $exception) {
		echo '{"text":'. $exception->getMessage() .'}'; 
	}
}

function addStablishment($idStablishmentAdmin) {
	//error_log('addWine\n', 3, '/var/tmp/php.log');
	$request = Slim::getInstance()->request();
	$body = $request->getBody();
	$stablishment = json_decode($body);
	$sqlAddStablishment = "INSERT INTO stablishment (name, address, description, image, status, stablishment_type_id, latitude, longitude, phone) VALUES (:name, :address,:description, :image, :status, :typeStablishmentId, :latitude, :longitude, :phone)";
	$sqlAddStablishmentAdminHasStablishment = "INSERT INTO stablishment_admin_has_stablishment (stablishment_id, stablishment_admin_id) VALUES (:stablishmentId,:adminId)";
	try {
		$db = getConnection();
		$queryBuilder = $db->prepare($sqlAddStablishment);  
		$queryBuilder->bindParam("name", $stablishment->name);
		$queryBuilder->bindParam("description", $stablishment->description);
		$queryBuilder->bindParam("address", $stablishment->address);
		$queryBuilder->bindParam("image", $stablishment->image);
		$queryBuilder->bindParam("status", $stablishment->status);
		$queryBuilder->bindParam("typeStablishmentId", $stablishment->stablishmentType->id);
		$queryBuilder->bindParam("latitude", $stablishment->latitude);
		$queryBuilder->bindParam("longitude", $stablishment->longitude);
		$queryBuilder->bindParam("phone", $stablishment->phone);
		$queryBuilder->execute();
		$stablishment->id = $db->lastInsertId();
		
		$queryBuilder = $db->prepare($sqlAddStablishmentAdminHasStablishment);
		$queryBuilder->bindParam("stablishmentId", $stablishment->id);
		$queryBuilder->bindParam("adminId", $idStablishmentAdmin);
		$queryBuilder->execute();
		
		$db = null;
		
		
	    echo json_encode($stablishment);
	} catch(PDOException $exception) {
		echo '{"text":'. $exception->getMessage() .'}'; 
	}
}

function updateStablishment($idStablishment) {
	$request = Slim::getInstance()->request();
	$body = $request->getBody();
	$stablishment = json_decode($body);
	$sqlUpdateStablishment = "UPDATE stablishment SET name=:name, address=:address, description=:description, image=:image, status=:status, stablishment_type_id=:typeStablishmentId, latitude=:latitude, longitude=:longitude, phone=:phone WHERE id=:id";
	try {
		$db = getConnection();
		$queryBuilder = $db->prepare($sqlUpdateStablishment);  
		$queryBuilder->bindParam("name", $stablishment->name);
		$queryBuilder->bindParam("description", $stablishment->description);
		$queryBuilder->bindParam("address", $stablishment->address);
		$queryBuilder->bindParam("image", $stablishment->image);
		$queryBuilder->bindParam("status", $stablishment->status);
		$queryBuilder->bindParam("typeStablishmentId", $stablishment->stablishmentType->id);
		$queryBuilder->bindParam("latitude", $stablishment->latitude);
		$queryBuilder->bindParam("longitude", $stablishment->longitude);
		$queryBuilder->bindParam("phone", $stablishment->phone);
		$queryBuilder->bindParam("id", $idStablishment);
		$queryBuilder->execute();
		$db = null;
		echo json_encode($stablishment); 
	} catch(PDOException $exception) {
		echo '{"text":'. $exception->getMessage() .'}'; 
	}
}

function deleteStablishment($idStablishment) {
    $request = Slim::getInstance()->request();
    $body = $request->getBody();
    $stablishment = json_decode($body);
	$sqlDeleteStablishment = "UPDATE stablishment SET status=:status WHERE id=:id";
	try {
		$db = getConnection();
		$queryBuilder = $db->prepare($sqlDeleteStablishment); 
		$queryBuilder->bindParam("status", $stablishment->status);
		$queryBuilder->bindParam("id", $idStablishment);
		$queryBuilder->execute();
		$db = null;
	} catch(PDOException $exception) {
		echo '{"text":'. $exception->getMessage() .'}'; 
	}
}

?>