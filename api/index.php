<?php

define('ACTIVE',1);
define('INACTIVE',0);
define('NOT_ERROR',0);


require 'Slim/Slim.php';

$app = new Slim();
$app->status(200);
$app->contentType('application/json');

include "StablishmentMapper.php";
include "StablishmentAdminMapper.php";
include "StablishmentTypeMapper.php";
include "UserMapper.php";
include "UserTypeMapper.php";
include "ClientMapper.php";
include "CouponMapper.php";
include "dbConnection.php";

$app->run();

?>