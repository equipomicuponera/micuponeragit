<?php

$app->get('/user', 'getUsers');
$app->get('/user/:idUser',	'getUser');
$app->get('/validateUsername/:username', 'validateUsername');
$app->get('/validateUser/:username/:password', 'validateUser');
$app->post('/user', 'addUser');
$app->put('/user/:idUser', 'updateUser');
$app->delete('/user/:idUser',	'deleteUser');

function validateUser($username, $password) {
    $encryptPassword = sha1($password); 
	$sqlValidateUsername = "SELECT * FROM user WHERE BINARY username=:username AND password=:password";
	try {
		$db = getConnection();
		$queryBuilder = $db->prepare($sqlValidateUsername);
		$queryBuilder->bindParam("username", $username);
		$queryBuilder->bindParam("password", $encryptPassword);
		$queryBuilder->execute();
		$user = $queryBuilder->fetchObject();
		$db = null;
		if($user){
			echo json_encode($user);
		}else{
			echo json_encode(array('success' => NOT_ERROR));
		}
	} catch(PDOException $exception) {
		echo '{"text":'. $exception->getMessage() .'}';
	}
}

function getUsers() {
	$sqlGetUsers = "SELECT * FROM user";
	try {
		$db = getConnection();
		$queryBuilder = $db->query($sqlGetUsers);  
		$users = $queryBuilder->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo json_encode($users);
	} catch(PDOException $exception) {
		echo '{"text":'. $exception->getMessage() .'}'; 
	}
}

function getUser($idUser) {
	$sqlGetUser = "SELECT * FROM user WHERE id=:id";
	try {
		$db = getConnection();
		$queryBuilder = $db->prepare($sqlGetUser);  
		$queryBuilder->bindParam("id", $idUser);
		$queryBuilder->execute();
		$user = $queryBuilder->fetchObject();  
		$db = null;
		echo json_encode($user); 
	} catch(PDOException $exception) {
		echo '{"text":'. $exception->getMessage() .'}'; 
	}
}

function validateUsername($username) {
	$sqlValidateUsername = "SELECT * FROM user WHERE username=:username";
	try {
		$db = getConnection();
		$queryBuilder = $db->prepare($sqlValidateUsername);
		$queryBuilder->bindParam("username", $username);
		$queryBuilder->execute();
		$user = $queryBuilder->fetchObject();
		$db = null;
		if($user){
			echo json_encode($user);
		}else{
			echo json_encode(array('error' => NOT_ERROR));
		}
		
	} catch(PDOException $exception) {
		echo '{"text":'. $exception->getMessage() .'}';
	}
}

function addUser() {
	//error_log('addWine\n', 3, '/var/tmp/php.log');
	$request = Slim::getInstance()->request();
	$body = $request->getBody();
	$user = json_decode($body);
	$sqlAddUser = "INSERT INTO user (username, password, user_type_id) VALUES (:username, :password, :userTypeId)";
	$encryptedPassword = sha1($user->password);
	try {
		$db = getConnection();
		$queryBuilder = $db->prepare($sqlAddUser);  
		$queryBuilder->bindParam("username", $user->username);
		$queryBuilder->bindParam("password", $encryptedPassword);
		$queryBuilder->bindParam("userTypeId", $user->userType->id);
		$queryBuilder->execute();
		$user->id = $db->lastInsertId();
		$db = null;
		
	    echo json_encode($user);
	} catch(PDOException $exception) {
		echo '{"text":'. $exception->getMessage() .'}'; 
	}
}

function updateUser($idUser) {
	$request = Slim::getInstance()->request();
	$body = $request->getBody();
	$user = json_decode($body);
	$sqlUpdateUser = "UPDATE user SET username=:username";
	
	if($user->password!="" || $user->password!=null){
	    $sqlUpdateUser .= ", password=:password";
	}
	$sqlUpdateUser .= " WHERE id=:id";
	
	try {
		$db = getConnection();
		$queryBuilder = $db->prepare($sqlUpdateUser);  
		$queryBuilder->bindParam("username", $user->username);
		$queryBuilder->bindParam("id", $idUser);
		
		if($user->password!="" || $user->password!=null){
		    $encryptedPassword = sha1($user->password);
		    $queryBuilder->bindParam("password", $encryptedPassword);
		}
		
		$queryBuilder->execute();
		$db = null;
		echo json_encode($user); 
	} catch(PDOException $exception) {
		echo '{"text":'. $exception->getMessage() .'}'; 
	}
}

function deleteUser($idUser) {
    $request = Slim::getInstance()->request();
    $body = $request->getBody();
    $user = json_decode($body);
	$sqlDeleteUser = "UPDATE user SET status=:status WHERE id=:id";
	try {
		$db = getConnection();
		$queryBuilder = $db->prepare($sqlDeleteUser); 
		$queryBuilder->bindParam("status", $user->status);
		$queryBuilder->bindParam("id", $idUser);
		$queryBuilder->execute();
		$db = null;
	} catch(PDOException $exception) {
		echo '{"text":'. $exception->getMessage() .'}'; 
	}
}



?>