<?php

$app->get('/stablishmentType', 'getStablishmentTypes');
$app->get('/stablishmentType/:idStablishmentType',	'getStablishmentType');

function getStablishmentTypes() {
	$sqlGetStablishmentTypes = "select * FROM stablishment_type ORDER BY id";
	try {
		$db = getConnection();
		$queryBuilder = $db->query($sqlGetStablishmentTypes);  
		$types = $queryBuilder->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo json_encode($types);
	} catch(PDOException $exception) {
		echo '{"error":'. $exception->getMessage() .'}'; 
	}
}

function getStablishmentType($idStablishmentType) {
	$sqlStablishmentType = "SELECT * FROM stablishment_type WHERE id=:id";
	try {
		$db = getConnection();
		$queryBuilder = $db->prepare($sqlStablishmentType);  
		$queryBuilder->bindParam("id", $idStablishmentType);
		$queryBuilder->execute();
		$type = $queryBuilder->fetchObject();  
		$db = null;
		echo json_encode($type); 
	} catch(PDOException $exception) {
		echo '{"error":'. $exception->getMessage() .'}'; 
	}
}

?>