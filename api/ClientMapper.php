<?php

$app->get('/client','getClients');
$app->get('/client/:idClient','getClient');
$app->get('/client/validateEmail/:email','validateEmailClient');
$app->get('/clientByUserId/:idUser',	'getClientByUserId');
$app->post('/client/:idUser', 'addClient');
$app->put('/client/:idClient', 'updateClient');
$app->delete('/client/:idClient',	'deleteClient');

function getClients() {
	$sqlGetClients = "SELECT * FROM client WHERE status=".ACTIVE."";
	try {
		$db = getConnection();
		$queryBuilder = $db->query($sqlGetClients);  
		$clients = $queryBuilder->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo json_encode($clients);
	} catch(PDOException $exception) {
		echo '{"text":'. $exception->getMessage() .'}'; 
	}
}

function getClient($idClient) {
	$sqlGetClient = "SELECT * FROM client WHERE id=:id";
	try {
		$db = getConnection();
		$queryBuilder = $db->prepare($sqlGetClient);  
		$queryBuilder->bindParam("id", $idClient);
		$queryBuilder->execute();
		$client = $queryBuilder->fetchObject();  
		$db = null;
		echo json_encode($client); 
	} catch(PDOException $exception) {
		echo '{"text":'. $exception->getMessage() .'}'; 
	}
}

function getClientByUserId($idUser) {
	$sqlGetClient = "SELECT * FROM client WHERE user_id=:id";
	try {
		$db = getConnection();
		$queryBuilder = $db->prepare($sqlGetClient);
		$queryBuilder->bindParam("id", $idUser);
		$queryBuilder->execute();
		$client = $queryBuilder->fetchObject();
		$db = null;
		echo json_encode($client);
	} catch(PDOException $exception) {
		echo '{"text":'. $exception->getMessage() .'}';
	}
}

function validateEmailClient($email) {
	$sqlValidateEmail = "SELECT * FROM client WHERE email=:email";
	try {
		$db = getConnection();
		$queryBuilder = $db->prepare($sqlValidateEmail);
		$queryBuilder->bindParam("email", $email);
		$queryBuilder->execute();
		$client = $queryBuilder->fetchObject();
		$db = null;
		if($client){
			echo json_encode($client);
		}else{
			echo json_encode(array('success' => NOT_ERROR));
		}
	} catch(PDOException $exception) {
		echo '{"text":'. $exception->getMessage() .'}';
	}
}

function addClient($idUser) {
	//error_log('addWine\n', 3, '/var/tmp/php.log');
	$request = Slim::getInstance()->request();
	$body = $request->getBody();
	$client = json_decode($body);
	$sqlAddClient = "INSERT INTO client (name, last_name, email, status, user_id) VALUES (:name, :lastname, :email, :status, :userId)";
	try {
		$db = getConnection();
		$queryBuilder = $db->prepare($sqlAddClient);  
		$queryBuilder->bindParam("name", $client->name);
		$queryBuilder->bindParam("lastname", $client->lastName);
		$queryBuilder->bindParam("email", $client->email);
		$queryBuilder->bindParam("status", $client->status);
		$queryBuilder->bindParam("userId", $idUser);
		$queryBuilder->execute();
		$client->id = $db->lastInsertId();
		
		$db = null;
		if ($client) {
			echo json_encode($client);
		}else{
			echo json_encode(array('error' => 0));
		}
	    
	} catch(PDOException $exception) {
		echo '{"text":'. $exception->getMessage() .'}'; 
	}
}

function updateClient($idClient) {
	$request = Slim::getInstance()->request();
	$body = $request->getBody();
	$client = json_decode($body);
	$sqlUpdateClient = "UPDATE client SET name=:name, last_name=:lastName, email=:email, status=:status WHERE id=:id";
	try {
		$db = getConnection();
		$queryBuilder = $db->prepare($sqlUpdateClient);  
		$queryBuilder->bindParam("name", $client->name);
		$queryBuilder->bindParam("lastName", $client->lastName);
		$queryBuilder->bindParam("email", $client->email);
		$queryBuilder->bindParam("status", $client->status);
		$queryBuilder->bindParam("id", $idClient);
		$queryBuilder->execute();
		$db = null;
		echo json_encode($client); 
	} catch(PDOException $exception) {
		echo '{"text":'. $exception->getMessage() .'}'; 
	}
}

function deleteClient($idClient) {
    $request = Slim::getInstance()->request();
    $body = $request->getBody();
    $client = json_decode($body);
	$sqlDeleteClient = "UPDATE client SET status=:status WHERE id=:id";
	try {
		$db = getConnection();
		$queryBuilder = $db->prepare($sqlDeleteClient); 
		$queryBuilder->bindParam("status", $client->status);
		$queryBuilder->bindParam("id", $idClient);
		$queryBuilder->execute();
		$db = null;
	} catch(PDOException $exception) {
		echo '{"text":'. $exception->getMessage() .'}'; 
	}
}

?>