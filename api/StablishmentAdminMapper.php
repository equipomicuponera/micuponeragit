<?php

$app->get('/stablishmentAdmin', 'getStablishmentAdmins');
$app->get('/stablishmentAdmin/:idStablishmentAdmin',	'getStablishmentAdmin');
$app->get('/stablishmentAdminByUserId/:idUser',	'getStablishmentAdminByUserId');
$app->get('/validateEmail/:email',	'validateEmail');
$app->post('/stablishmentAdmin/:idUser', 'addStablishmentAdmin');
$app->put('/stablishmentAdmin/:idStablishmentAdmin', 'updateStablishmentAdmin');
$app->delete('/stablishmentAdmin/:idStablishmentAdmin',	'deleteStablishmentAdmin');

function getStablishmentAdmins() {
	$sqlGetStablishmentAdmins = "SELECT * FROM stablishment_admin WHERE status=".ACTIVE."";
	try {
		$db = getConnection();
		$queryBuilder = $db->query($sqlGetStablishmentAdmins);  
		$stablishmentsAdmin = $queryBuilder->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo json_encode($stablishmentsAdmin);
	} catch(PDOException $exception) {
		echo '{"text":'. $exception->getMessage() .'}'; 
	}
}

function getStablishmentAdmin($idStablishmentAdmin) {
	$sqlGetStablishmentAdmin = "SELECT * FROM stablishment_admin WHERE id=:id";
	try {
		$db = getConnection();
		$queryBuilder = $db->prepare($sqlGetStablishmentAdmin);  
		$queryBuilder->bindParam("id", $idStablishmentAdmin);
		$queryBuilder->execute();
		$stablishmentAdmin = $queryBuilder->fetchObject();  
		$db = null;
		echo json_encode($stablishmentAdmin); 
	} catch(PDOException $exception) {
		echo '{"text":'. $exception->getMessage() .'}'; 
	}
}

function getStablishmentAdminByUserId($idUser) {
	$sqlGetStablishmentAdmin = "SELECT * FROM stablishment_admin WHERE user_id=:id";
	try {
		$db = getConnection();
		$queryBuilder = $db->prepare($sqlGetStablishmentAdmin);
		$queryBuilder->bindParam("id", $idUser);
		$queryBuilder->execute();
		$stablishmentAdmin = $queryBuilder->fetchObject();
		$db = null;
		echo json_encode($stablishmentAdmin);
	} catch(PDOException $exception) {
		echo '{"text":'. $exception->getMessage() .'}';
	}
}

function validateEmail($email) {
	$sqlValidateEmail = "SELECT * FROM stablishment_admin WHERE email=:email";
	try {
		$db = getConnection();
		$queryBuilder = $db->prepare($sqlValidateEmail);
		$queryBuilder->bindParam("email", $email);
		$queryBuilder->execute();
		$stablishmentAdmin = $queryBuilder->fetchObject();
		$db = null;
		echo json_encode($stablishmentAdmin);
	} catch(PDOException $exception) {
		echo '{"text":'. $exception->getMessage() .'}';
	}
}

function addStablishmentAdmin($idUser) {
	$request = Slim::getInstance()->request();
	$body = $request->getBody();
	$stablishmentAdmin = json_decode($body);
	$sqlAddStablishmentAdmin = "INSERT INTO stablishment_admin (name, last_name, email, status, user_id) VALUES (:name, :lastname, :email, :status, :userId)";
	try {
		$db = getConnection();
		$queryBuilder = $db->prepare($sqlAddStablishmentAdmin);  
		$queryBuilder->bindParam("name", $stablishmentAdmin->name);
		$queryBuilder->bindParam("lastname", $stablishmentAdmin->lastName);
		$queryBuilder->bindParam("email", $stablishmentAdmin->email);
		$queryBuilder->bindParam("status", $stablishmentAdmin->status);
		$queryBuilder->bindParam("userId", $idUser);
		$queryBuilder->execute();
		$stablishmentAdmin->id = $db->lastInsertId();
		
		$db = null;
	    echo json_encode($stablishmentAdmin);
	} catch(PDOException $exception) {
		echo '{"text":'. $exception->getMessage() .'}'; 
	}
}

function updateStablishmentAdmin($idStablishmentAdmin) {
	$request = Slim::getInstance()->request();
	$body = $request->getBody();
	$stablishmentAdmin = json_decode($body);
	$sqlUpdateStablishmentAdmin = "UPDATE stablishment_admin SET name=:name, last_name=:lastName, email=:email, status=:status WHERE id=:id";
	try {
		$db = getConnection();
		$queryBuilder = $db->prepare($sqlUpdateStablishmentAdmin);  
		$queryBuilder->bindParam("name", $stablishmentAdmin->name);
		$queryBuilder->bindParam("lastName", $stablishmentAdmin->lastName);
		$queryBuilder->bindParam("email", $stablishmentAdmin->email);
		$queryBuilder->bindParam("status", $stablishmentAdmin->status);
		$queryBuilder->bindParam("id", $idStablishmentAdmin);
		$queryBuilder->execute();
		$db = null;
		echo json_encode($stablishmentAdmin); 
	} catch(PDOException $exception) {
		echo '{"text":'. $exception->getMessage() .'}'; 
	}
}

function deleteStablishmentAdmin($idStablishmentAdmin) {
    $request = Slim::getInstance()->request();
    $body = $request->getBody();
    $stablishmentAdmin = json_decode($body);
	$sqlDeleteStablishmentAdmin = "UPDATE stablishment_admin SET status=:status WHERE id=:id";
	try {
		$db = getConnection();
		$queryBuilder = $db->prepare($sqlDeleteStablishmentAdmin); 
		$queryBuilder->bindParam("status", $stablishmentAdmin->status);
		$queryBuilder->bindParam("id", $idStablishmentAdmin);
		$queryBuilder->execute();
		$db = null;
	} catch(PDOException $exception) {
		echo '{"text":'. $exception->getMessage() .'}'; 
	}
}

?>