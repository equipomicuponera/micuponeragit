<?php

$app->get('/coupons/:idStablishment', 'getCouponsByStablishment');
$app->get('/coupons/client/:idClient', 'getCouponsByClient');
$app->get('/coupons/notAcquired/client/:idClient', 'getCouponsNotAcquiredByClient');
$app->get('/coupons/notAcquired/:idClient/:idStablishment', 'getStablishmentsCouponsNotAcquiredByClient');
$app->get('/coupon/:idCoupon',	'getCoupon');
$app->get('/coupon/addClientCoupon/:idCoupon/:idClient', 'addClientCoupon');
$app->get('/coupon/exchangeClientCoupon/:idCoupon/:idClient', 'exchangeCoupon');
$app->get('/coupon/increaseUsedValue/:idCoupon', 'increaseUsedValue');
$app->post('/coupon/:idStablishment', 'addCoupon');
$app->put('/coupon/:idCoupon', 'updateCoupon');
$app->delete('/coupon/:idCoupon',	'deleteCoupon');

function getCouponsByStablishment($idStablishment) {
    $sqlGetCoupons = "SELECT coupon.id, coupon.name, coupon.start_date, coupon.end_date, coupon.image, coupon.description, coupon.status, coupon.qr_image, stablishment_has_coupon.coupon_id, stablishment_has_coupon.stablishment_id, stablishment_has_coupon.downloaded, stablishment_has_coupon.acquired, stablishment_has_coupon.used, stablishment_type.name as stablishmentTypeName FROM coupon JOIN stablishment_has_coupon ON coupon.id = stablishment_has_coupon.coupon_id JOIN stablishment ON stablishment.id = stablishment_has_coupon.stablishment_id JOIN stablishment_type ON stablishment_type.id = stablishment.stablishment_type_id WHERE coupon.status=".ACTIVE." AND stablishment_has_coupon.stablishment_id=".$idStablishment;
    try {
        $db = getConnection();
        $queryBuilder = $db->query($sqlGetCoupons);
        $coupon = $queryBuilder->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo json_encode($coupon);
    } catch(PDOException $exception) {
        echo '{"text":'. $exception->getMessage() .'}';
    }
}

function getAcquiredCouponByClient($idClient){
    $sqlClientCoupons = "SELECT coupon_id FROM client_has_coupon WHERE client_id = ".$idClient;
    $idAcquiredCoupons = array();
    try {
    	$db = getConnection();
    	$queryBuilder = $db->query($sqlClientCoupons);
    	$idsCoupons = $queryBuilder->fetchAll(PDO::FETCH_OBJ);
    
    	foreach ($idsCoupons as $element){
    		if(!in_array($element->coupon_id,$idAcquiredCoupons)){
    			array_push($idAcquiredCoupons,$element->coupon_id);
    		}
    	}
    	
    	$clientHasAcquiredCoupons = (count($idAcquiredCoupons)>0)?true:false;
    	
    	if($clientHasAcquiredCoupons){
    	    $query = "(";
    	    $query .= implode(",",$idAcquiredCoupons);
    	    $query .= ")";
    	}else{
    	    $query = null;
    	}
    
    	$db = null;
    	
    	return $query;
    } catch(PDOException $exception) {
    	return null;
    }
}

function getCouponsNotAcquiredByClient($idClient) {
	try {
	    $queryIdsCouponsAcquired = getAcquiredCouponByClient($idClient);
		$db = getConnection();
		$today = Date("Y-m-d");
		$sqlGetNotAcquiredCoupons = "SELECT stablishment_type.name as stablishmentTypeName,stablishment_has_coupon.stablishment_id,stablishment.stablishment_type_id,stablishment_has_coupon.downloaded,stablishment_has_coupon.used,stablishment_has_coupon.acquired,coupon.id as id,coupon.name, start_date, end_date, coupon.image, coupon.description, coupon.status, qr_image FROM coupon JOIN stablishment_has_coupon ON coupon.id = stablishment_has_coupon.coupon_id JOIN stablishment ON stablishment.id = stablishment_has_coupon.stablishment_id JOIN stablishment_type ON stablishment_type.id = stablishment.stablishment_type_id WHERE coupon.status=".ACTIVE." AND DATE_FORMAT(start_date, '%Y-%m-%d') <= '".$today."' AND DATE_FORMAT(end_date, '%Y-%m-%d') >= '".$today."' AND stablishment_has_coupon.downloaded < stablishment_has_coupon.acquired";
		
		if($queryIdsCouponsAcquired!=null){
		    $sqlGetNotAcquiredCoupons.= " AND coupon.id NOT IN ".$queryIdsCouponsAcquired;
		}
		
		$queryBuilder = $db->query($sqlGetNotAcquiredCoupons);
		$coupons = $queryBuilder->fetchAll(PDO::FETCH_OBJ);
		
		$db = null;
		echo json_encode($coupons);
	} catch(PDOException $exception) {
		echo '{"text":'. $exception->getMessage() .'}';
	}
}

function getStablishmentsCouponsNotAcquiredByClient($idClient,$idStablishment) {
	try {
		$queryIdsCouponsAcquired = getAcquiredCouponByClient($idClient);
		$db = getConnection();
		$today = Date("Y-m-d");
		$sqlGetNotAcquiredCoupons = "SELECT stablishment_type.name as stablishmentTypeName,stablishment_has_coupon.stablishment_id,stablishment.stablishment_type_id,stablishment_has_coupon.downloaded,stablishment_has_coupon.used,stablishment_has_coupon.acquired,coupon.id as id,coupon.name, start_date, end_date, coupon.image, coupon.description, coupon.status, qr_image FROM coupon JOIN stablishment_has_coupon ON coupon.id = stablishment_has_coupon.coupon_id JOIN stablishment ON stablishment.id = stablishment_has_coupon.stablishment_id JOIN stablishment_type ON stablishment_type.id = stablishment.stablishment_type_id WHERE coupon.status=".ACTIVE." AND DATE_FORMAT(start_date, '%Y-%m-%d') <= '".$today."' AND DATE_FORMAT(end_date, '%Y-%m-%d') >= '".$today."' AND stablishment_has_coupon.downloaded < stablishment_has_coupon.acquired AND stablishment.id = ".$idStablishment;

		if($queryIdsCouponsAcquired!=null){
			$sqlGetNotAcquiredCoupons.= " AND coupon.id NOT IN ".$queryIdsCouponsAcquired;
		}

		$queryBuilder = $db->query($sqlGetNotAcquiredCoupons);
		$coupons = $queryBuilder->fetchAll(PDO::FETCH_OBJ);

		$db = null;
		echo json_encode($coupons);
	} catch(PDOException $exception) {
		echo '{"text":'. $exception->getMessage() .'}';
	}
}

function getCouponsByClient($idClient) {
	$sqlGetCoupons = "SELECT coupon.id,coupon.name, coupon.start_date, coupon.end_date, coupon.image, coupon.description, coupon.qr_image, coupon.status, client_has_coupon.used as used, stablishment_type.name as stablishmentTypeName, stablishment_has_coupon.downloaded, stablishment_has_coupon.acquired, stablishment_has_coupon.stablishment_id, stablishment_has_coupon.coupon_id FROM coupon JOIN client_has_coupon ON coupon.id = client_has_coupon.coupon_id JOIN stablishment_has_coupon ON coupon.id = stablishment_has_coupon.coupon_id JOIN stablishment ON stablishment.id = stablishment_has_coupon.stablishment_id JOIN stablishment_type ON stablishment_type.id = stablishment.stablishment_type_id WHERE coupon.status=".ACTIVE." AND client_has_coupon.client_id=".$idClient;
	try {
		$db = getConnection();
		$queryBuilder = $db->query($sqlGetCoupons);
		$coupon = $queryBuilder->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo json_encode($coupon);
	} catch(PDOException $exception) {
		echo '{"text":'. $exception->getMessage() .'}';
	}
}

function getCoupon($idCoupon) {
    $sqlGetCoupon = "SELECT * FROM coupon JOIN stablishment_has_coupon ON coupon.id = stablishment_has_coupon.coupon_id WHERE coupon.id=:id";
    try {
        $db = getConnection();
        $queryBuilder = $db->prepare($sqlGetCoupon);
        $queryBuilder->bindParam("id", $idCoupon);
        $queryBuilder->execute();
        $coupon = $queryBuilder->fetchObject();
        $db = null;
        echo json_encode($coupon);
    } catch(PDOException $exception) {
        echo '{"text":'. $exception->getMessage() .'}';
    }
}

function addCoupon($idStablishment) {
    $request = Slim::getInstance()->request();
    $body = $request->getBody();
    $coupon = json_decode($body);
    $sqlAddCoupon = "INSERT INTO coupon (name, start_date, end_date, image, description, status) VALUES (:name, :startDate,:endDate, :image, :description, :status)";
    $sqlAddStablishmentHasCoupon = "INSERT INTO stablishment_has_coupon (coupon_id, stablishment_id, downloaded, acquired, used) VALUES (:couponId,:stablishmentId,:downloaded,:acquired,:used)";
    try {
        $db = getConnection();
        $queryBuilder = $db->prepare($sqlAddCoupon);
        $queryBuilder->bindParam("name", $coupon->name);
        $queryBuilder->bindParam("startDate", $coupon->startDate);
        $queryBuilder->bindParam("endDate", $coupon->endDate);
        $queryBuilder->bindParam("image", $coupon->image);
        $queryBuilder->bindParam("description", $coupon->description);
        $queryBuilder->bindParam("status", $coupon->status);
        $queryBuilder->execute();
        $coupon->id = $db->lastInsertId();

        $queryBuilder = $db->prepare($sqlAddStablishmentHasCoupon);
        $queryBuilder->bindParam("couponId", $coupon->id);
        $queryBuilder->bindParam("stablishmentId", $idStablishment);
        $queryBuilder->bindParam("downloaded", $coupon->downloaded);
        $queryBuilder->bindParam("acquired", $coupon->acquired);
        $queryBuilder->bindParam("used", $coupon->used);
        $queryBuilder->execute();

        $db = null;


        echo json_encode($coupon);
    } catch(PDOException $exception) {
        echo '{"text":'. $exception->getMessage() .'}';
    }
}

function updateCoupon($idCoupon) {
    $request = Slim::getInstance()->request();
    $body = $request->getBody();
    $coupon = json_decode($body);
    $sqlUpdateCoupon = "UPDATE coupon SET qr_image=:qrImage WHERE id=:id";
    try {
        $db = getConnection();
        $queryBuilder = $db->prepare($sqlUpdateCoupon);
        $queryBuilder->bindParam("qrImage", $coupon->qrImage);
        $queryBuilder->bindParam("id", $idCoupon);
        $queryBuilder->execute();
        $db = null;
        echo json_encode($coupon);
    } catch(PDOException $exception) {
        echo '{"text":'. $exception->getMessage() .'}';
    }
}

function addClientCoupon($idCoupon,$idClient) {
	$request = Slim::getInstance()->request();
	$body = $request->getBody();
	$coupon = json_decode($body);
	$unusedStatus = 0;
	$sqlAddClientCoupon = "INSERT INTO client_has_coupon (client_id,coupon_id, used) VALUES (".$idClient.",".$idCoupon.",".$unusedStatus.")";
	try {
		$db = getConnection();
		$queryBuilder = $db->prepare($sqlAddClientCoupon);
		$executionResult = $queryBuilder->execute();
		$db = null;
		
		$resultPlus = increaseDownloadedValue($idCoupon);

		if($executionResult && $resultPlus){
			echo json_encode(array('response' => true));
		}else{
			echo json_encode(array('response' => false));
		}
	} catch(PDOException $exception) {
		echo '{"text":'. $exception->getMessage() .'}';
	}
}

function exchangeCoupon($idCoupon,$idClient) {
	$request = Slim::getInstance()->request();
	$body = $request->getBody();
	$coupon = json_decode($body);
	$sqlUpdateCoupon = "UPDATE client_has_coupon SET used=true WHERE client_id=".$idClient." AND coupon_id=".$idCoupon;
	try {
		$db = getConnection();
		$queryBuilder = $db->prepare($sqlUpdateCoupon);
		$executionResult = $queryBuilder->execute();
		$db = null;
		
		if($executionResult){
		  echo json_encode(array('response' => true));
		}else{
			echo json_encode(array('response' => false));
		}
	} catch(PDOException $exception) {
		echo '{"text":'. $exception->getMessage() .'}';
	}
}

function increaseDownloadedValue($idCoupon) {
    $sqlGetCoupon = "SELECT * FROM coupon JOIN stablishment_has_coupon ON coupon.id = stablishment_has_coupon.coupon_id WHERE coupon.id=:id";
    try {
        $db = getConnection();
        $queryBuilder = $db->prepare($sqlGetCoupon);
        $queryBuilder->bindParam("id", $idCoupon);
        $queryBuilder->execute();
        $coupon = $queryBuilder->fetchObject();
        
        static $DOWNLOADED_COUPON = 1;
        
        $downloadedTotal = $coupon->downloaded + $DOWNLOADED_COUPON;
        $sqlUpdateCoupon = "UPDATE stablishment_has_coupon JOIN coupon ON coupon.id = stablishment_has_coupon.coupon_id SET stablishment_has_coupon.downloaded=:downloaded WHERE stablishment_has_coupon.coupon_id=:id";
        
        $queryBuilder = $db->prepare($sqlUpdateCoupon);
        $queryBuilder->bindParam("id", $idCoupon);
        $queryBuilder->bindParam("downloaded", $downloadedTotal);
        $executionResult = $queryBuilder->execute();
        
        $db = null;
        return $executionResult;
    } catch(PDOException $exception) {
        echo '{"text":'. $exception->getMessage() .'}';
    }
}

function increaseUsedValue($idCoupon) {
	$sqlGetCoupon = "SELECT * FROM coupon JOIN stablishment_has_coupon ON coupon.id = stablishment_has_coupon.coupon_id WHERE coupon.id=:id";
	try {
		$db = getConnection();
		$queryBuilder = $db->prepare($sqlGetCoupon);
		$queryBuilder->bindParam("id", $idCoupon);
		$queryBuilder->execute();
		$coupon = $queryBuilder->fetchObject();
		
		static $USED_COUPON = 1;

		$usedTotal = $coupon->used + $USED_COUPON;
		$sqlUpdateCoupon = "UPDATE stablishment_has_coupon JOIN coupon ON coupon.id = stablishment_has_coupon.coupon_id SET stablishment_has_coupon.used=:used WHERE stablishment_has_coupon.coupon_id=:id";

		$queryBuilder = $db->prepare($sqlUpdateCoupon);
		$queryBuilder->bindParam("id", $idCoupon);
		$queryBuilder->bindParam("used", $usedTotal);
		$executionResult = $queryBuilder->execute();

		$db = null;
	   if($executionResult){
		  echo json_encode(array('response' => true));
		}else{
			echo json_encode(array('response' => false));
		}
	} catch(PDOException $exception) {
		echo '{"text":'. $exception->getMessage() .'}';
	}
}

function deleteCoupon($idCoupon) {
	$request = Slim::getInstance()->request();
	$body = $request->getBody();
	$coupon = json_decode($body);
	$sqlDeleteCoupon = "UPDATE coupon SET status=:status WHERE id=:id";
	try {
		$db = getConnection();
		$queryBuilder = $db->prepare($sqlDeleteCoupon);
		$queryBuilder->bindParam("status", $coupon->status);
		$queryBuilder->bindParam("id", $idCoupon);
		$queryBuilder->execute();
		$db = null;
	} catch(PDOException $exception) {
		echo '{"text":'. $exception->getMessage() .'}';
	}
}

?>