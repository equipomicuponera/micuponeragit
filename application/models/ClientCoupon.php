<?php

class Application_Model_ClientCoupon extends Application_Model_Abstract
{

    public $coupon;
    public $used;

    /**
     *
     * @return the $coupon
     */
    public function getCoupon ()
    {
        return $this->coupon;
    }

    /**
     *
     * @return the $used
     */
    public function getUsed ()
    {
        return $this->used;
    }

    /**
     *
     * @param field_type $coupon            
     */
    public function setCoupon ($coupon)
    {
        $this->coupon = $coupon;
    }

    /**
     *
     * @param field_type $used            
     */
    public function setUsed ($used)
    {
        $this->used = $used;
    }
}