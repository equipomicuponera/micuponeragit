<?php

class Application_Model_Stablishment extends Application_Model_Abstract
{

    public $id;
    public $name;
    public $address;
    public $description;
    public $image;
    public $status;
    public $latitude;
    public $longitude;
    public $phone;
    public $stablishmentType;
    public $coupons;

    /**
     *
     * @return the $id
     */
    public function getId ()
    {
        return $this->id;
    }

    /**
     *
     * @return the $name
     */
    public function getName ()
    {
        return $this->name;
    }

    /**
     *
     * @return the $address
     */
    public function getAddress ()
    {
        return $this->address;
    }

    /**
     *
     * @return the $description
     */
    public function getDescription ()
    {
        return $this->description;
    }

    /**
     *
     * @return the $image
     */
    public function getImage ()
    {
        return $this->image;
    }

    /**
     *
     * @return the $status
     */
    public function getStatus ()
    {
        return $this->status;
    }

    /**
     *
     * @return the $latitude
     */
    public function getLatitude ()
    {
        return $this->latitude;
    }

    /**
     *
     * @return the $longitude
     */
    public function getLongitude ()
    {
        return $this->longitude;
    }

    /**
     *
     * @return the $phone
     */
    public function getPhone ()
    {
        return $this->phone;
    }

    /**
     *
     * @return the $stablishmentType
     */
    public function getStablishmentType ()
    {
        return $this->stablishmentType;
    }

    /**
     *
     * @param field_type $id            
     */
    public function setId ($id)
    {
        $this->id = $id;
    }

    /**
     *
     * @param field_type $name            
     */
    public function setName ($name)
    {
        $this->name = $name;
    }

    /**
     *
     * @param field_type $address            
     */
    public function setAddress ($address)
    {
        $this->address = $address;
    }

    /**
     *
     * @param field_type $description            
     */
    public function setDescription ($description)
    {
        $this->description = $description;
    }

    /**
     *
     * @param field_type $image            
     */
    public function setImage ($image)
    {
        $this->image = $image;
    }

    /**
     *
     * @param field_type $status            
     */
    public function setStatus ($status)
    {
        $this->status = $status;
    }

    /**
     *
     * @param field_type $latitude            
     */
    public function setLatitude ($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     *
     * @param field_type $longitude            
     */
    public function setLongitude ($longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     *
     * @param field_type $phone            
     */
    public function setPhone ($phone)
    {
        $this->phone = $phone;
    }

    /**
     *
     * @param field_type $stablishmentType            
     */
    public function setStablishmentType ($stablishmentType)
    {
        $this->stablishmentType = $stablishmentType;
    }

    /**
     *
     * @return the $coupons
     */
    public function getCoupons ()
    {
        return $this->coupons;
    }

    /**
     *
     * @param field_type $coupons            
     */
    public function setCoupons ($coupons)
    {
        $this->coupons = $coupons;
    }
}

