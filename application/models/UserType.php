<?php

class Application_Model_UserType extends Application_Model_Abstract
{

    public $id;
    public $name;

    public static $USER_TYPE_ARRAY = array(
            "SUPERADMIN" => 1,
            "ADMIN" => 2,
            "CLIENT" => 3
    );

    /**
     *
     * @return the $name
     */
    public function getName ()
    {
        return $this->name;
    }

    /**
     *
     * @param field_type $name            
     */
    public function setName ($name)
    {
        $this->name = $name;
    }

    /**
     *
     * @return the $id
     */
    public function getId ()
    {
        return $this->id;
    }

    /**
     *
     * @param field_type $id            
     */
    public function setId ($id)
    {
        $this->id = $id;
    }
}

?>