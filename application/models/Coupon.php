<?php

class Application_Model_Coupon extends Application_Model_Abstract
{

    public $id;
    public $name;
    public $startDate;
    public $endDate;
    public $image;
    public $description;
    public $status;
    public $qrImage;
    public $downloaded;
    public $acquired;
    public $used;

    /**
     *
     * @return the $qrImage
     */
    public function getQrImage ()
    {
        return $this->qrImage;
    }

    /**
     *
     * @param field_type $qrImage            
     */
    public function setQrImage ($qrImage)
    {
        $this->qrImage = $qrImage;
    }

    /**
     *
     * @return the $downloaded
     */
    public function getDownloaded ()
    {
        return $this->downloaded;
    }

    /**
     *
     * @return the $acquired
     */
    public function getAcquired ()
    {
        return $this->acquired;
    }

    /**
     *
     * @return the $used
     */
    public function getUsed ()
    {
        return $this->used;
    }

    /**
     *
     * @param field_type $downloaded            
     */
    public function setDownloaded ($downloaded)
    {
        $this->downloaded = $downloaded;
    }

    /**
     *
     * @param field_type $acquired            
     */
    public function setAcquired ($acquired)
    {
        $this->acquired = $acquired;
    }

    /**
     *
     * @param field_type $used            
     */
    public function setUsed ($used)
    {
        $this->used = $used;
    }

    /**
     *
     * @return the $id
     */
    public function getId ()
    {
        return $this->id;
    }

    /**
     *
     * @return the $name
     */
    public function getName ()
    {
        return $this->name;
    }

    /**
     *
     * @return the $startDate
     */
    public function getStartDate ()
    {
        return $this->startDate;
    }

    /**
     *
     * @return the $endDate
     */
    public function getEndDate ()
    {
        return $this->endDate;
    }

    /**
     *
     * @return the $image
     */
    public function getImage ()
    {
        return $this->image;
    }

    /**
     *
     * @return the $description
     */
    public function getDescription ()
    {
        return $this->description;
    }

    /**
     *
     * @return the $status
     */
    public function getStatus ()
    {
        return $this->status;
    }

    /**
     *
     * @param field_type $id            
     */
    public function setId ($id)
    {
        $this->id = $id;
    }

    /**
     *
     * @param field_type $name            
     */
    public function setName ($name)
    {
        $this->name = $name;
    }

    /**
     *
     * @param field_type $startDate            
     */
    public function setStartDate ($startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     *
     * @param field_type $endDate            
     */
    public function setEndDate ($endDate)
    {
        $this->endDate = $endDate;
    }

    /**
     *
     * @param field_type $image            
     */
    public function setImage ($image)
    {
        $this->image = $image;
    }

    /**
     *
     * @param field_type $description            
     */
    public function setDescription ($description)
    {
        $this->description = $description;
    }

    /**
     *
     * @param field_type $status            
     */
    public function setStatus ($status)
    {
        $this->status = $status;
    }
}