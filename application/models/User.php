<?php

class Application_Model_User extends Application_Model_Abstract
{

    public $id;
    public $username;
    public $password;
    public $userType;

    /**
     *
     * @return the $id
     */
    public function getId ()
    {
        return $this->id;
    }

    /**
     *
     * @return the $username
     */
    public function getUsername ()
    {
        return $this->username;
    }

    /**
     *
     * @return the $password
     */
    public function getPassword ()
    {
        return $this->password;
    }

    /**
     *
     * @return the $userType
     */
    public function getUserType ()
    {
        return $this->userType;
    }

    /**
     *
     * @param field_type $id            
     */
    public function setId ($id)
    {
        $this->id = $id;
    }

    /**
     *
     * @param field_type $username            
     */
    public function setUsername ($username)
    {
        $this->username = $username;
    }

    /**
     *
     * @param field_type $password            
     */
    public function setPassword ($password)
    {
        $this->password = $password;
    }

    /**
     *
     * @param field_type $userType            
     */
    public function setUserType ($userType)
    {
        $this->userType = $userType;
    }
}

