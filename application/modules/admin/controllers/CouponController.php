<?php

class Admin_CouponController extends Zend_Controller_Action
{

    public function preDispatch ()
    {
        if (!Zend_Session::namespaceIsset("cuponera")) {
            $this->_redirect('/login');
        } else {
            $this->view->controller = 'coupon';
        }
    }

    public function indexAction ()
    {
        $idStablishment = Application_Service_Encrypter::decrypter($this->getParam('id'));
        $couponService = new Application_Service_Coupon();
        $coupons = $couponService->getAllCouponsByStablishment($idStablishment);
        
        $stablishmentService = new Application_Service_Stablishment();
        $stablishment = $stablishmentService->getOneStablishmentById($idStablishment);
        
        $this->view->coupons = $coupons;
        $this->view->stablishment = $stablishment;
    }

    public function newAction ()
    {
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getParams();
            $adapterLogo = new Zend_File_Transfer_Adapter_Http();
            
            $coupon = new Application_Model_Coupon();
            $coupon->createFromArray($data);
            
            // Para la fecha
            $dateStart = $this->getRequest()->getParam('startDate');
            $dateEnd = $this->getRequest()->getParam('endDate');
            $coupon = $this->setCouponDates($dateStart, $dateEnd, $coupon);
            
            // Para el Logo
            $coupon = $this->setCouponLogo($coupon);
            
            $stablishmentId = $data["stablishment"];
            
            $coupon->setStatus($this->ACTIVE);
            $coupon->setDownloaded($this->DOWNLOADED_INITIAL_VALUE);
            $coupon->setUsed($this->USED_INITIAL_VALUE);
            
            $couponService = new Application_Service_Coupon();
            $couponElement = $couponService->addCoupon($coupon, $stablishmentId);
            
            $this->updateQRImage($couponService, $couponElement);
            
            $this->_helper->redirector->gotoSimple('index', 'coupon', null, 
                    array(
                            'id' => Application_Service_Encrypter::encrypter($stablishmentId)
                    ));
        } else {
            $stablishmentService = new Application_Service_Stablishment();
            $session = new Zend_Session_Namespace('cuponera');
            $stalishments = $stablishmentService->getAllStablishmentsByAdmin($session->stablishmentAdmin->getId());
            $this->view->stablishment = $stalishments;
        }
    }

    public function seeCouponAction ()
    {
        $idCoupon = Application_Service_Encrypter::decrypter($this->getRequest()->getParam('id'));
        
        $couponService = new Application_Service_Coupon();
        $coupon = $couponService->getOneCouponById($idCoupon);
        $coupon = $this->setVisibleDatesOnCoupon($coupon);
        
        $this->view->coupon = $coupon;
    }
    
    private function setCouponDates($startDate, $endDate, $coupon){
        $startDateAsString = $this->getDateAsString($startDate);
        $endDateAsString = $this->getDateAsString($endDate);
        $coupon->setStartDate($startDateAsString);
        $coupon->setEndDate($endDateAsString);
        return $coupon;
    }
    
    private function getDateAsString($unformattedDate){
        $HOUR = 0;
        $MINUTE = 0;
        $SECOND = 0;
        $MONTH_POSITION = 1;
        $DAY_POSITION = 2;
        $YEAR_POSITION = 0;
        $dateArray = explode("-", $unformattedDate);
        $dateAsString = date('Y-m-d', mktime($HOUR, $MINUTE, $SECOND, $dateArray[$MONTH_POSITION], $dateArray[$DAY_POSITION], $dateArray[$YEAR_POSITION]));
        return $dateAsString;
    }
    
    private function setVisibleDatesOnCoupon($coupon){
        $dateService = new Application_Service_Date();
        $startDate = $dateService->getDate($coupon->getStartDate());
        $endDate = $dateService->getDate($coupon->getEndDate());
        
        $coupon->setStartDate($startDate);
        $coupon->setEndDate($endDate);
        return $coupon;
    }
    
    private function setCouponLogo($coupon){
        $adapterLogo = new Zend_File_Transfer_Adapter_Http();
        $newNameFile = time();
        $fileName = $adapterLogo->getFileName('image', false);
        
        if ($fileName != null) {
            $fileNameExplode = explode(".", $fileName);
            $fileExtensionPosition = count($fileNameExplode) - 1;
            $extension = $fileNameExplode[$fileExtensionPosition];
        
            $adapterLogo->setDestination(realpath(APPLICATION_PATH . '/../public/logos/coupon'));
            $adapterLogo->addFilter('Rename',
                    array(
                            'target' => APPLICATION_PATH .
                            '/../public/logos/coupon/' .
                            $newNameFile . '.' . $extension,
                            'overwrite' => true
                    ));
        
            if ($adapterLogo->receive($fileName)) {
                $coupon->setImage($newNameFile . '.' . $extension, false);
            } else {
                $coupon->setImage('default.png');
            }
        } else {
            $coupon->setImage('default.png');
        }
        return $coupon;
    }

    private function updateQRImage ($couponService, $couponElement)
    {
        // Para el código qr
        include (APPLICATION_PATH . '/../library/phpqrcode/qrlib.php');
        $coupon = new Application_Model_Coupon();
        // ruta para guardar los pngs
        $qrImageRoute = APPLICATION_PATH . '/../public/logos/coupon/qrimages/';
        
        // Cambiando el string, el qr cambia y se genera una nueva imagen
        
        $couponId = $couponElement->id;
        $codeContents = $couponId;
        
        // se genera el nombre del archivo con md5
        $qrImageName = '001_file_' . md5($codeContents) . '.png';
        
        $pngAbsoluteFilePath = $qrImageRoute . $qrImageName;

        $QR_CODE_COMPLEXITY_LEVEL = 10;
        if (!file_exists($pngAbsoluteFilePath)) {
            QRcode::png($codeContents, $pngAbsoluteFilePath, QR_ECLEVEL_L, $QR_CODE_COMPLEXITY_LEVEL);
            $coupon->setQrImage($qrImageName, false);
        } else {
            $coupon->setQrImage('default.png');
        }
        
        $coupon->setId($couponId);
        $couponService->updateCoupon($coupon);
    }

    private $ACTIVE = 1;
    private $INACTIVE = 0;
    private $DOWNLOADED_INITIAL_VALUE = 0;
    private $USED_INITIAL_VALUE = 0;
}
