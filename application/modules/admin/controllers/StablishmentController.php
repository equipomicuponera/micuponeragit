<?php

class Admin_StablishmentController extends Zend_Controller_Action
{

    public function preDispatch ()
    {
        if (! Zend_Session::namespaceIsset("cuponera")) {
            $this->_redirect('/login');
        } else {
            $this->view->controller = 'stablishment';
        }
    }

    public function indexAction ()
    {
        $session = new Zend_Session_Namespace('cuponera');
        $stablishmentService = new Application_Service_Stablishment();
        $stablishments = $stablishmentService->getAllStablishmentsByAdmin($session->stablishmentAdmin->getId());
        $this->view->stablishments = $stablishments;
    }

    public function newAction ()
    {
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getParams();
            
            $stablishment = new Application_Model_Stablishment();
            $stablishment->createFromArray($data);
            
            $stablishment = $this->uploadStablishmentLogo($stablishment);
            
            $stablishmentTypeService = new Application_Service_StablishmentType();
            $stablishmentType = $stablishmentTypeService->getOneStablishmentTypeById($data["stablishment_type"]);
            
            $stablishment->setStablishmentType($stablishmentType);
            $stablishment->setStatus($this->ACTIVE);
            
            $stablishmentService = new Application_Service_Stablishment();
            $stablishmentCreated = $stablishmentService->addStablishment($stablishment);
            
            $this->_helper->redirector->gotoSimple('see-stablishment', 'stablishment', null,
            		array(
            				'id' =>  Application_Service_Encrypter::encrypter($stablishmentCreated->id)
            		));
        } else {
            $stablishmentTypeService = new Application_Service_StablishmentType();
            $stablishmentTypes = $stablishmentTypeService->getAllStablishmentTypes();
            $this->view->stablishmentType = $stablishmentTypes;
        }
    }

    public function editAction ()
    {
        $idStablishment = Application_Service_Encrypter::decrypter($this->getRequest()->getParam('id'));
        
        $stablishmentService = new Application_Service_Stablishment();
        $stablishment = $stablishmentService->getOneStablishmentById($idStablishment);
        
        $stablishmentTypeService = new Application_Service_StablishmentType();
        $stablishmentTypes = $stablishmentTypeService->getAllStablishmentTypes();
        $this->view->stablishmentType = $stablishmentTypes;
        $this->view->stablishment = $stablishment;
    }

    public function seeStablishmentAction ()
    {
        $idStablishment = Application_Service_Encrypter::decrypter($this->getRequest()->getParam('id'));
        
        $stablishmentService = new Application_Service_Stablishment();
        $stablishment = $stablishmentService->getOneStablishmentById($idStablishment);
        
        $this->view->stablishment = $stablishment;
    }

    public function updateAction ()
    {
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getParams();
            
            $stablishmentService = new Application_Service_Stablishment();
            $stablishment = $stablishmentService->getOneStablishmentById($data["id_stablishment"]);
            $stablishment->createFromArray($data);
            $stablishment->setId($data["id_stablishment"]);
            
            $stablishment = $this->uploadUpdatedStablishmentLogo($stablishment);
            
            $stablishmentTypeService = new Application_Service_StablishmentType();
            $stablishmentType = $stablishmentTypeService->getOneStablishmentTypeById($data["stablishment_type"]);
            
            $stablishment->setStablishmentType($stablishmentType);
            $stablishment->setStatus($this->ACTIVE);
            
            $stablishmentService = new Application_Service_Stablishment();
            $stablishmentUpdated = $stablishmentService->updateStablishment($stablishment);
            
            $this->_helper->redirector->gotoSimple('see-stablishment', 'stablishment', null,
            		array(
            				'id' => Application_Service_Encrypter::encrypter($stablishmentUpdated->id)
            		));
        }
    }

    public function deleteAction ()
    {
        $idStablishment = $this->getRequest()->getParam('id');
        
        $couponService = new Application_Service_Coupon();
        $coupons = $couponService->getAllCouponsByStablishment($idStablishment);
        
        $stablishment = new Application_Model_Stablishment();
        $stablishment->setId($idStablishment);
        $stablishment->setStatus($this->INACTIVE);
        $stablishment->setCoupons($coupons);
        
        $stablishmentService = new Application_Service_Stablishment();
        $stablishmentService->deleteStablishment($stablishment);
        
        $this->_helper->redirector('index');
    }
    
    public function uploadStablishmentLogo($stablishment){
        $adapterLogo = new Zend_File_Transfer_Adapter_Http();
        // Para el Logo
        $newNameFile = time();
        $fileName = $adapterLogo->getFileName('logo', false);
        
        if ($fileName != null) {
        	$fileNameExplode = explode(".", $fileName);
        	$fileExtensionPosition = count($fileNameExplode) - 1;
            $extension = $fileNameExplode[$fileExtensionPosition];
        
        	$adapterLogo->setDestination(realpath(APPLICATION_PATH.'/../public/logos/stablishment'));
        	$adapterLogo->addFilter('Rename',
        			array(
        					'target' => APPLICATION_PATH .
        					'/../public/logos/stablishment/' .
        					$newNameFile . '.' . $extension,
        					'overwrite' => true
        			));
        
        	if ($adapterLogo->receive($fileName)) {
        		$stablishment->setImage($newNameFile . '.' . $extension, false);
        	} else {
        		$stablishment->setImage('default.png');
        	}
        } else {
        	$stablishment->setImage('default.png');
        }
        return $stablishment;
    }
    
    public function uploadUpdatedStablishmentLogo($stablishment){
        $newNameFile = time();
        
        $adapter = new Zend_File_Transfer_Adapter_Http();
        $fileName = $adapter->getFileName('logo', false);
        $oldFile = $stablishment->getImage();
        
        if ($fileName != null) {
        	$fileNameExplode = explode(".", $fileName);
        	$fileExtensionPosition = count($fileNameExplode) - 1;
            $extension = $fileNameExplode[$fileExtensionPosition];
        
        	$adapter->setDestination(realpath(APPLICATION_PATH .'/../public/logos/stablishment'));
        	$adapter->addFilter('Rename',
        			array(
        					'target' => APPLICATION_PATH .
        					'/../public/logos/stablishment/' .
        					$newNameFile . '.' . $extension,
        					'overwrite' => true
        			));
        
        	if ($adapter->receive()) {
        		$stablishment->setImage($adapter->getFileName('logo', false));
        		if ($oldFile != "default.png") {
        			unlink(realpath(APPLICATION_PATH .'/../public/logos/stablishment/' .$oldFile));
        		}
        	} else {
        		$stablishment->setImage('default.png');
        	}
        } else {
        	$stablishment->setImage($oldFile);
        }
        return $stablishment;
    }
    
    private $ACTIVE = 1;
    private $INACTIVE = 0;
}