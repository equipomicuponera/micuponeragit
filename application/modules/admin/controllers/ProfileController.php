<?php

class Admin_ProfileController extends Zend_Controller_Action
{

    public function preDispatch ()
    {
        if (! Zend_Session::namespaceIsset("cuponera")) {
            $this->_redirect('/login');
        } else {
            $this->view->controller = 'profile';
        }
    }

    public function indexAction ()
    {
    }
    
    public function editprofileAction ()
    {
    }
    
    public function updateAction ()
    {
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getParams();
            
            $user = new Application_Model_User();
            $user->setId($data["idUser"]);
            $user->setUsername($data["username"]);
            $user->setPassword($data["password"]);
            
            $stablishmentAdmin = new Application_Model_StablishmentAdmin();
            $stablishmentAdmin->setId($data["idStablishmentAdmin"]);
            $stablishmentAdmin->setEmail($data["email"]);
            $stablishmentAdmin->setName($data["name"]);
            $stablishmentAdmin->setLastName($data["lastName"]);
            $stablishmentAdmin->setStatus($this->ACTIVE);
            $stablishmentAdmin->setUser($user);
            
            $userService = new Application_Service_User();
            $userService->updateUser($user);
            
            $stablishmentAdminService = new Application_Service_StablishmentAdmin();
            $stablishmentAdminService->updateStablishmentAdmin($stablishmentAdmin);
            
            $session = new Zend_Session_Namespace('cuponera');
            $session->stablishmentAdmin = $stablishmentAdmin;
            
            $this->_helper->redirector('index');
        }  
    }
    
    public function validateUsernameAction()
    {
    	if ($this->getRequest()->isPost()) {
    	    $this->_helper->layout()->disableLayout();
    	    $this->_helper->viewRenderer->setNoRender(true);
    		
            $data = $this->getRequest()->getParams();
            $idUser = $data["idUser"];
    
    		$userService = new Application_Service_User();
            $user = $userService->existUsername($data["username"]);
            
    		if($user!=null){//existe
                if($user->getId() == $idUser){
                    echo $this->ACCEPTED;
                } else{
                    echo $this->NOT_ACCEPTED;
                }    		    
    		} else{
    		    echo $this->ACCEPTED;
    		}
    	}
    }
    
    public function validateEmailAction()
    {
    	if ($this->getRequest()->isPost()) {
    	    $this->_helper->layout()->disableLayout();
    	    $this->_helper->viewRenderer->setNoRender(true);
    		
            $data = $this->getRequest()->getParams();
            $idStablishmentAdmin = $data["idStablishmentAdmin"];
    
    		$stablishmentAdminService = new Application_Service_StablishmentAdmin();
            $stablishmentAdmin = $stablishmentAdminService->existEmail($data["email"]);
            
    		if($stablishmentAdmin!=null){
    			if($stablishmentAdmin->getId() == $idStablishmentAdmin){
                    echo $this->ACCEPTED;
                } else{
                    echo $this->NOT_ACCEPTED;
                }
    		} else{
    			echo $this->ACCEPTED;
    		}
    	}
    }
    
    private $NOT_ACCEPTED = 1;
    private $ACCEPTED = 0;
    private $ACTIVE = 1;
    
}