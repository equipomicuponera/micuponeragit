<?php
class Superadmin_IndexController extends Zend_Controller_Action
{
    public function preDispatch()
    {
    	if (!Zend_Session::namespaceIsset("cuponera")) {
            $this->_redirect('/login');
        }
    }
    
    public function init()
    {
    	/* Initialize action controller here */
    	$this->view->controller = 'index';
    	$this->view->action = $this->getRequest()->getActionName();
    }
    /**
     * The default action - show the home page
     */
    public function indexAction(){
        
    }
    
  
    
}

?>