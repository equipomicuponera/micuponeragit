<?php
class Superadmin_StablishmentAdminController extends Zend_Controller_Action {

	public function preDispatch(){
		if (!Zend_Session::namespaceIsset("cuponera")) {
            $this->_redirect('/login');
        }
		else{
			$this->view->controller = 'stablishmentadmin';
		}
	}

	public function indexAction(){
	    $stablishmentAdminService = new Application_Service_StablishmentAdmin();
	    $stablishmentAdmins = $stablishmentAdminService->getAllStablishmentAdmin();
	    $this->view->stablishmentAdmins = $stablishmentAdmins;
	}
	
    public function stablishmentCouponListAction() 
    {
        $stablishmentId = $this->getRequest()->getParam('id');
        
        $couponService = new Application_Service_Coupon();
	    $coupons = $couponService->getAllCouponsByStablishment($stablishmentId);
	    
	    $stablishmentService = new Application_Service_Stablishment();
	    $stablishment = $stablishmentService->getOneStablishmentById($stablishmentId);

	    $this->view->coupons = $coupons;
	    $this->view->stablishment = $stablishment;
    }
	
	public function seeStablishmentAction () {
		$idStablishment = $this->getRequest()->getParam('id');
	
		$stablishmentService = new Application_Service_Stablishment();
		$stablishment = $stablishmentService->getOneStablishmentById($idStablishment);
	
		$this->view->stablishment = $stablishment;
	}
	
	public function seeStablishmentAdminAction () {
	    
		$idStablishmentAdmin = $this->getRequest()->getParam('id');
	
		$stablishmentAdminService = new Application_Service_StablishmentAdmin();
		$stablishmentAdmin = $stablishmentAdminService->getOneStablishmentAdminById($idStablishmentAdmin);
		
		$stablishmentService = new Application_Service_Stablishment();
		$stablishments = $stablishmentService->getAllStablishmentsByAdmin($idStablishmentAdmin);
		
		$this->view->stablishments = $stablishments;
		$this->view->stablishmentAdmin = $stablishmentAdmin;
	}
	
	public function seeCouponAction () {
	    $idCoupon = $this->getRequest()->getParam('id');
	
	    $couponService = new Application_Service_Coupon();
	    $coupon = $couponService->getOneCouponById($idCoupon);
	
	    $dateService = new Application_Service_Date();
	    $startDate = $dateService->getDate($coupon->getStartDate());
	    $endDate = $dateService->getDate($coupon->getEndDate());
	     
	    $coupon->setStartDate($startDate);
	    $coupon->setEndDate($endDate);
	    
	    $this->view->coupon = $coupon;
	}
	
	public function deleteAction () {
		$idStablishmentAdmin = $this->getRequest()->getParam('id');
		
		$stablishmentService = new Application_Service_Stablishment();
		$couponService = new Application_Service_Coupon();
		
		$stablishments = $stablishmentService->getAllStablishmentsByAdmin($idStablishmentAdmin);
		
		foreach ($stablishments as $stablishment){
		    $stablishment->setStatus($this->INACTIVE);
		    $coupons = $couponService->getAllCouponsByStablishment($stablishment->getId());
		    $stablishment->setCoupons($coupons);
		    $stablishmentService->deleteStablishment($stablishment);
		}
	
		$stablishment = new Application_Model_StablishmentAdmin();
		$stablishment->setId($idStablishmentAdmin);
		$stablishment->setStatus($this->INACTIVE);
	
		$stablishmentService = new Application_Service_StablishmentAdmin();
		$stablishmentService->deleteStablishmentAdmin($stablishment);
	
		$this->_helper->redirector('index');
	}
	
	private $INACTIVE = 0;
}