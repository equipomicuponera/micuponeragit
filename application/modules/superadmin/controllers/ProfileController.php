<?php

class SuperAdmin_ProfileController extends Zend_Controller_Action
{

    public function preDispatch ()
    {
        if (! Zend_Session::namespaceIsset("cuponera")) {
            $this->_redirect('/login');
        } else {
            $this->view->controller = 'profile';
        }
    }

    public function indexAction ()
    {   
    }
    
    public function editprofileAction ()
    {
    }
    
    public function updateAction ()
    {
        if ($this->getRequest()->isPost()) {        
            $data = $this->getRequest()->getParams();
            
            $user = new Application_Model_User();
            $user->setId($data["idUser"]);
            $user->setUsername($data["username"]);
            $user->setPassword($data["password"]);
            
            $userService = new Application_Service_User();
            $userService->updateuser($user);
            
            $session = new Zend_Session_Namespace('cuponera');
            $session->SuperAdmin = $user;
            
            $this->_helper->redirector('index');
        } 
    }
    
    public function validateUsernameAction()
    {
    	if ($this->getRequest()->isPost()) {
    	    $this->_helper->layout()->disableLayout();
    	    $this->_helper->viewRenderer->setNoRender(true);
    		
            $data = $this->getRequest()->getParams();
            $idUser = $data["idUser"];
    
    		$userService = new Application_Service_User();
            $user = $userService->existUsername($data["username"]);
            
    		if($user!=null){//existe
                if($user->getId() == $idUser){
                    echo $this->ACCEPTED;
                } else{
                    echo $this->NOT_ACCEPTED;
                } 
    		} else{
    		    echo $this->ACCEPTED;
    		}
    	}
    }

    private $NOT_ACCEPTED = 1;
    private $ACCEPTED = 0;
    private $ACTIVE = 1;
}