<?php
class Superadmin_ClientController extends Zend_Controller_Action {
    
	public function preDispatch(){
		if (!Zend_Session::namespaceIsset("cuponera")) {
            $this->_redirect('/login');
        }
		else{
			$this->view->controller = 'client';
		}
	}

	public function indexAction(){
	    $clientService = new Application_Service_Client();
	    $clients = $clientService->getAllClients();
	    $this->view->clients = $clients;
	}
	
	public function seeClientAction () {
		$idClient = $this->getRequest()->getParam('id');
	
		$clientService = new Application_Service_Client();
		$client = $clientService->getOneClientById($idClient);
		
		$couponService = new Application_Service_Coupon();
		$coupons = $couponService->getAllCouponsByClient($idClient);
		
		$this->view->coupons = $coupons;
		$this->view->client = $client;
	}
	
	public function deleteAction () {
		$idClient = $this->getRequest()->getParam('id');
	
		$client = new Application_Model_Client();
		$client->setId($idClient);
		$client->setStatus(0);
	
		$clientService = new Application_Service_Client();
		$clientService->deleteClient($client);
	
		$this->_helper->redirector('index');
	}
}