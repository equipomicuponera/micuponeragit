<?php

class Application_Service_User
{
    
    public function addUser($user){
        
        $jsonData = json_encode($user);
        	
        $restService = new Application_Service_REST();
        $response = $restService->callWebService($this->url."user", $jsonData, 'POST');
        $newUser = json_decode($response);
        return $newUser;
        
    }
    
    public function updateUser($user){
    
    	$jsonData = json_encode($user);
    	 
    	$restService = new Application_Service_REST();
    	$response = $restService->callWebService($this->url."user/".$user->getId(), $jsonData, 'PUT');
    	$updatedUser = json_decode($response);
    	return $updatedUser;
    
    }
    
    public function deleteUser($user){
    
    	$jsonData = json_encode($user);
    
    	$restService = new Application_Service_REST();
    	$response = $restService->callWebService($this->url."user/".$user->getId(), $jsonData, 'DELETE');
    	$deletedUser = json_decode($response);
    	return $deletedUser;
    
    }
    
    public function getAllUsers(){
        $users = array();
        $restService = new Application_Service_REST();
        $response = $restService->callWebService($this->url."user", null, 'GET');
        	$userList = json_decode($response);
        	foreach ($userList as $userElement){
        	    $user = new Application_Model_User();
        	    $user->setId($userElement->id);
        	    $user->setUsername($userElement->username);
        	    $user->setPassword($userElement->password);
        	    
        	    $userTypeService = new Application_Service_UserType();
        	    $userType = $userTypeService->getOneUserTypeById($userElement->user_type_id);
        	    
        	    $user->setUserType($userType);
        	    
        	    array_push($users, $user);
        	}
        return $users;
    }
    
    public function getOneUserById($idUser){
        $restService = new Application_Service_REST();
        $response = $restService->callWebService($this->url."user/".$idUser, null, 'GET');
        $userElement = json_decode($response);
                $user = new Application_Model_User();
        	    $user->setId($userElement->id);
        	    $user->setUsername($userElement->username);
        	    $user->setPassword($userElement->password);
        	    
        	    $userTypeService = new Application_Service_UserType();
        	    $userType = $userTypeService->getOneUserTypeById($userElement->user_type_id);
        	    
        	    $user->setUserType($userType);
        return $user;
    }
    
    public function existUsername($username){
    	$restService = new Application_Service_REST();
    	$response = $restService->callWebService($this->url."validateUsername/".$username, null, 'GET');
    	 $userElement = json_decode($response);
    	
    	if(!isset($userElement->error)){//existe un usuario
    	    $user = new Application_Model_User();
    	    
        	 $user->setId($userElement->id);
        	 $user->setUsername($userElement->username);
        	 $user->setPassword($userElement->password);
        	    
        	 $userTypeService = new Application_Service_UserType();
        	 $userType = $userTypeService->getOneUserTypeById($userElement->user_type_id);
        	   
        	 $user->setUserType($userType);
             return $user;
    	}else{//no existe
    	    return null;
    	}
    }
    
    public function getUserByUsernameAndPassword($username,$password){
    	$restService = new Application_Service_REST();
    	$response = $restService->callWebService($this->url."validateUser/".$username."/".$password, null, 'GET');
    	$userElement = json_decode($response);
    	
    	if(!isset($userElement->success)){//existe un usuario
    		    $user = new Application_Model_User();
        	    $user->setId($userElement->id);
        	    $user->setUsername($userElement->username);
        	    $user->setPassword($userElement->password);
        	    
        	    $userTypeService = new Application_Service_UserType();
        	    $userType = $userTypeService->getOneUserTypeById($userElement->user_type_id);
        	    
        	    $user->setUserType($userType);
        	    return $user;
    	}else{//no existe
    		return null;
    	}
    }
    
    private $url = MAIN_URL_API;
}

?>