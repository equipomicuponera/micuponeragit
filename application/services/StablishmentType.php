<?php

class Application_Service_StablishmentType
{
    
    public function getAllStablishmentTypes(){
        $stablishmentTypesArray = array();
        $restService = new Application_Service_REST();
        $response = $restService->callWebService($this->url."stablishmentType", null, 'GET');
        	$stablishmentTypeList = json_decode($response);
        	foreach ($stablishmentTypeList as $stablishmentTypeElement){
        	    $stablishmentType = new Application_Model_StablishmentType();
        	    $stablishmentType->setId($stablishmentTypeElement->id);
        	    $stablishmentType->setName($stablishmentTypeElement->name);
        	    array_push($stablishmentTypesArray, $stablishmentType);
        	}
        return $stablishmentTypesArray;
    }
    
    public function getOneStablishmentTypeById($idStablishmentType){
        $restService = new Application_Service_REST();
        $response = $restService->callWebService($this->url."stablishmentType/".$idStablishmentType, null, 'GET');
        $stablishmentTypeElement = json_decode($response);
        $stablishmentType = new Application_Model_StablishmentType();
        $stablishmentType->setId($stablishmentTypeElement->id);
        $stablishmentType->setName($stablishmentTypeElement->name);
        return $stablishmentType;
    }
    
    private $url = MAIN_URL_API;
}

?>