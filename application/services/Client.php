<?php

class Application_Service_Client
{
    
    public function addClient($client){
        
        $jsonData = json_encode($client);
        	
        $userService = new Application_Service_User();
        $id = $userService->addUser($client->getUser());
        
         $restService = new Application_Service_REST();
         $response = $restService->callWebService($this->url."client/".$id, $jsonData, 'POST');
         $newClient = json_decode($response);
         return $newClient;
        
    }
    
    public function updateClient($client){
    
    	$jsonData = json_encode($client);
    	 
    	$restService = new Application_Service_REST();
    	$response = $restService->callWebService($this->url."client/".$client->getId(), $jsonData, 'PUT');
    	$updatedClient = json_decode($response);
    	return $updatedClient;
    
    }
    
    public function deleteClient($client){
    
    	$jsonData = json_encode($client);
    
    	$restService = new Application_Service_REST();
    	$response = $restService->callWebService($this->url."client/".$client->getId(), $jsonData, 'DELETE');
    	$deletedClient = json_decode($response);
    	return $deletedClient;
    
    }
    
    public function getAllClients(){
        $clientsArray = array();
        $restService = new Application_Service_REST();
        $response = $restService->callWebService($this->url."client", null, 'GET');
        $clientList = json_decode($response);
        	foreach ($clientList as $clientElement){
        	    $client = new Application_Model_Client();
        	    $client->setId($clientElement->id);
        	    $client->setName($clientElement->name);
        	    $client->setLastName($clientElement->last_name);
        	    $client->setEmail($clientElement->email);
        	    $client->setStatus($clientElement->status);
        	    
        	    $userService = new Application_Service_User();
        	    $user = $userService->getOneUserById($clientElement->user_id);
        	    
        	    $client->setUser($user);
        	    
        	    array_push($clientsArray, $client);
        	}
        return $clientsArray;
    }
    
    public function getOneClientById($idClient){
        $restService = new Application_Service_REST();
        $response = $restService->callWebService($this->url."client/".$idClient, null, 'GET');
        $clientElement = json_decode($response);
                $client = new Application_Model_Client();
        	    $client->setId($clientElement->id);
        	    $client->setName($clientElement->name);
        	    $client->setLastName($clientElement->last_name);
        	    $client->setEmail($clientElement->email);
        	    $client->setStatus($clientElement->status);
        	    
        	    $userService = new Application_Service_User();
        	    $user = $userService->getOneUserById($clientElement->user_id);
        	    
        	    $client->setUser($user);
        return $client;
    }
    
    public function existEmail($email){
    	$restService = new Application_Service_REST();
    	$response = $restService->callWebService($this->url."client/validateEmail/".$email, null, 'GET');
    	$booleanResponse = json_decode($response);
    	 
    	if($booleanResponse!=false){//existe un usuario
    		return true;
    	}else{//no existe
    		return false;
    	}
    }
    
    private $url = MAIN_URL_API;
}

?>