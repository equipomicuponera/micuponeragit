<?php

class Application_Service_Coupon
{

    public function addCoupon($coupon, $idStablishment){

        $jsonData = json_encode($coupon);
         
        $restService = new Application_Service_REST();
        $response = $restService->callWebService($this->url."coupon/".$idStablishment, $jsonData, 'POST');
        $newCoupon = json_decode($response);
        return $newCoupon;

    }

    public function updateCoupon($coupon){

        $jsonData = json_encode($coupon);

        $restService = new Application_Service_REST();
        $response = $restService->callWebService($this->url."coupon/".$coupon->getId(), $jsonData, 'PUT');
        $updatedCoupon = json_decode($response);
        return $updatedCoupon;

    }
    
    public function getAllCouponsByStablishment($idStablishment){
        $couponsByStablishmentArray = array();
        $restService = new Application_Service_REST();
        $response = $restService->callWebService($this->url."coupons/".$idStablishment, null, 'GET');
        $couponList = json_decode($response);
        foreach ($couponList as $couponElement){
            $coupon = new Application_Model_Coupon();
            $coupon->setId($couponElement->coupon_id);
            $coupon->setName($couponElement->name);
            $coupon->setStartDate($couponElement->start_date);
            $coupon->setEndDate($couponElement->end_date);
            $coupon->setImage($couponElement->image);
            $coupon->setDescription($couponElement->description);
            $coupon->setStatus($couponElement->status);
            $coupon->setQrImage($couponElement->qr_image);
            $coupon->setDownloaded($couponElement->downloaded);
            $coupon->setAcquired($couponElement->acquired);
            $coupon->setUsed($couponElement->used);
             
            array_push($couponsByStablishmentArray, $coupon);
        }
        return $couponsByStablishmentArray;
    }
    
    public function getAllCouponsByClient($idClient){
    	$couponsByClientArray = array();
    	$restService = new Application_Service_REST();
    	$response = $restService->callWebService($this->url."coupons/client/".$idClient, null, 'GET');
    	$couponList = json_decode($response);
    	foreach ($couponList as $couponElement){
    		$coupon = new Application_Model_Coupon();
    		$coupon->setId($couponElement->coupon_id);
    		$coupon->setName($couponElement->name);
    		$coupon->setStartDate($couponElement->start_date);
    		$coupon->setEndDate($couponElement->end_date);
    		$coupon->setImage($couponElement->image);
    		$coupon->setDescription($couponElement->description);
    		$coupon->setStatus($couponElement->status);
    		$coupon->setQrImage($couponElement->qr_image);
    		
    		$clientCoupon = new Application_Model_ClientCoupon();
    		$clientCoupon->setUsed($couponElement->used);
    		$clientCoupon->setCoupon($coupon);
    		 
    		array_push($couponsByClientArray, $clientCoupon);
    	}
    	return $couponsByClientArray;
    }
    
    public function getOneCouponById($idCoupon){
        $restService = new Application_Service_REST();
        $response = $restService->callWebService($this->url."coupon/".$idCoupon, null, 'GET');
        $couponElement = json_decode($response);
        $coupon = new Application_Model_Coupon();
        $coupon->setId($couponElement->id);
        $coupon->setName($couponElement->name);
        $coupon->setStartDate($couponElement->start_date);
        $coupon->setEndDate($couponElement->end_date);
        $coupon->setImage($couponElement->image);
        $coupon->setDescription($couponElement->description);
        $coupon->setStatus($couponElement->status);
        $coupon->setQrImage($couponElement->qr_image);
        $coupon->setDownloaded($couponElement->downloaded);
        $coupon->setAcquired($couponElement->acquired);
        $coupon->setUsed($couponElement->used);
        
        return $coupon;
    }
    
    public function deleteCoupon($coupon){
    
    	$jsonData = json_encode($coupon);
    	
    	$restService = new Application_Service_REST();
    	$response = $restService->callWebService($this->url."coupon/".$coupon->getId(), $jsonData, 'DELETE');
    	$deletedCoupon = json_decode($response);
    	return $deletedCoupon;
    
    }
    
    private $url = MAIN_URL_API;
}

?>