<?php

class Application_Service_Stablishment
{
    
    public function addStablishment($stablishment){
        
        $session = new Zend_Session_Namespace('cuponera');
        $idStablishmentAdmin = $session->stablishmentAdmin->getId();
        
        $jsonData = json_encode($stablishment);
        
        $restService = new Application_Service_REST();
        $response = $restService->callWebService($this->url."stablishment/".$idStablishmentAdmin, $jsonData, 'POST');
        $newStablishment = json_decode($response);
        return $newStablishment;
        
    }
    
    public function updateStablishment($stablishment){
    
    	$jsonData = json_encode($stablishment);
    	 
    	$restService = new Application_Service_REST();
    	$response = $restService->callWebService($this->url."stablishment/".$stablishment->getId(), $jsonData, 'PUT');
    	$updatedStablishment = json_decode($response);
    	return $updatedStablishment;
    
    }
    
    public function deleteStablishment($stablishment){
    
    	$jsonData = json_encode($stablishment);
    	
    	$couponService = new Application_Service_Coupon();
    	$coupons = $stablishment->getCoupons();
    	foreach ($coupons as $coupon){
    	    $coupon->setStatus($this->INACTIVE);
    	    $couponService->deleteCoupon($coupon);
    	}
    	
    
    	$restService = new Application_Service_REST();
    	$response = $restService->callWebService($this->url."stablishment/".$stablishment->getId(), $jsonData, 'DELETE');
    	$deletedStablishment = json_decode($response);
    	return $deletedStablishment;
    
    }
    
    public function getAllStablishmentsByAdmin($idStablishmentAdmin){
        $stablishmentsByAdminArray = array();
        $restService = new Application_Service_REST();
        $response = $restService->callWebService($this->url."stablishments/".$idStablishmentAdmin, null, 'GET');
        	$stablishmentList = json_decode($response);
        	foreach ($stablishmentList as $stablishmentElement){
        	    $stablishment = new Application_Model_Stablishment();
        	    $stablishment->setId($stablishmentElement->id);
        	    $stablishment->setName($stablishmentElement->name);
        	    $stablishment->setDescription($stablishmentElement->description);
        	    $stablishment->setAddress($stablishmentElement->address);
        	    $stablishment->setImage($stablishmentElement->image);
        	    $stablishment->setLatitude($stablishmentElement->latitude);
        	    $stablishment->setLongitude($stablishmentElement->longitude);
        	    $stablishment->setPhone($stablishmentElement->phone);
        	    
        	    $stablishmentTypeService = new Application_Service_StablishmentType();
        	    $stablishmentType = $stablishmentTypeService->getOneStablishmentTypeById($stablishmentElement->stablishment_type_id);
        	    
        	    $stablishment->setStablishmentType($stablishmentType);
        	    
        	    array_push($stablishmentsByAdminArray, $stablishment);
        	}
        return $stablishmentsByAdminArray;
    }
    
    public function getOneStablishmentById($idStablishment){
        $restService = new Application_Service_REST();
        $response = $restService->callWebService($this->url."stablishment/".$idStablishment, null, 'GET');
        $stablishmentElement = json_decode($response);
                $stablishment = new Application_Model_Stablishment();
        	    $stablishment->setId($stablishmentElement->id);
        	    $stablishment->setName($stablishmentElement->name);
        	    $stablishment->setDescription($stablishmentElement->description);
        	    $stablishment->setAddress($stablishmentElement->address);
        	    $stablishment->setImage($stablishmentElement->image);
        	    $stablishment->setLatitude($stablishmentElement->latitude);
        	    $stablishment->setLongitude($stablishmentElement->longitude);
        	    $stablishment->setPhone($stablishmentElement->phone);
        	    
        	    $stablishmentTypeService = new Application_Service_StablishmentType();
        	    $stablishmentType = $stablishmentTypeService->getOneStablishmentTypeById($stablishmentElement->stablishment_type_id);
        	    
        	    $stablishment->setStablishmentType($stablishmentType);
        return $stablishment;
    }
    
    private $url = MAIN_URL_API;
    private $ACTIVE = 1;
    private $INACTIVE = 0;
}

?>