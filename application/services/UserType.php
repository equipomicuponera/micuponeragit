<?php

class Application_Service_UserType
{
    
    public function getAllUserTypes(){
        $userTypesArray = array();
        $restService = new Application_Service_REST();
        $response = $restService->callWebService($this->url."userType", null, 'GET');
        	$userTypeList = json_decode($response);
        	foreach ($userTypeList as $userTypeElement){
        	    $userType = new Application_Model_UserType();
        	    $userType->setId($userTypeElement->id);
        	    $userType->setName($userTypeElement->name);
        	    array_push($userTypesArray, $userType);
        	}
        return $userTypesArray;
    }
    
    public function getOneUserTypeById($idUserType){
        $restService = new Application_Service_REST();
        $response = $restService->callWebService($this->url."userType/".$idUserType, null, 'GET');
        $userTypeElement = json_decode($response);
        $userType = new Application_Model_UserType();
        $userType->setId($userTypeElement->id);
        $userType->setName($userTypeElement->name);
        return $userType;
    }
    
    private $url = MAIN_URL_API;
}

?>