<?php

class Application_Service_REST
{
    
    public function callWebService($url,$jsonData,$typeMethod){
        
        try {
            $post = file_get_contents($url,null,stream_context_create(array(
            		'http' => array(
            				'protocol_version' => 1.1,
            				'user_agent'       => 'CuponeraWeb',
            				'method'           => $typeMethod,
            				'header'           => "Content-type: application/json\r\n".
            				"Connection: close\r\n" .
            				"Content-length: " . strlen($jsonData) . "\r\n",
            				'content'          => $jsonData,
            		),
            )));
            return $post;
        }catch(Exception $e){
            echo "Ocurri&oacute; un error al recuperar la informaci&oacute;n ".$e->getMessage().".";
        }
        
        
    }
}

?>