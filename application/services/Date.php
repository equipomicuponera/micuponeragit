<?php
class Application_Service_Date{
    public static $months = array("Enero"=>1,"Febrero"=>2,"Marzo"=>3,"Abril"=>4,"Mayo"=>5,"Junio"=>6,"Julio"=>7,"Agosto"=>8,"Septiembre"=>9,"Octubre"=>10,"Noviembre"=>11,"Diciembre"=>12);
    public static $days = array("Domingo"=>0,"Lunes"=>1,"Martes"=>2,"Miercoles"=>3,"Jueves"=>4,"Viernes"=>5,"Sábado"=>6);
    
    public static function getDate($dateStamp){
        
        $days = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
        
        $dateTimeStamp = strtotime($dateStamp);
        $year = date('Y',$dateTimeStamp);
        $month = date('n',$dateTimeStamp);
        $day = date('d',$dateTimeStamp);
        $dayOfWeek = date('w',$dateTimeStamp);
    
        return $day." / ".array_search($month, self::$months). " / ".$year;
    }
}