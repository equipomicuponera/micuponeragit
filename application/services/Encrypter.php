<?php

class Application_Service_Encrypter {
	
	public static function encrypter($data){
		return urlencode(base64_encode($data));
	}
	
	public static function decrypter($data){
		return base64_decode(urldecode($data));
	}
	
	public static function encryptData($data, $key)
	{
		$sResult = '';
		if ($key != '' && $data != '') {
			for ($i = 0; $i < strlen($data); $i++) {
				$sChar = substr($data, $i, 1);
				$sKeyChar = substr($key, ($i % strlen($key)) - 1, 1);
				$sChar = chr(ord($sChar) + ord($sKeyChar));
				$sResult .= $sChar;
			}
		}
		return self::encode_base64($sResult);
	}
	
	public static function decryptData($data, $key)
	{
		$sResult = '';
		$data = self::decode_base64($data);
		for ($i = 0; $i < strlen($data); $i++) {
			$sChar = substr($data, $i, 1);
			$sKeyChar = substr($key, ($i % strlen($key)) - 1, 1);
			$sChar = chr(ord($sChar) - ord($sKeyChar));
			$sResult .= $sChar;
		}
		$sResult = str_replace("<", "&lt;", $sResult);
		$sResult = str_replace(">", "&gt;", $sResult);
	
		return $sResult;
	}
	
	private static function encode_base64($sData){
		$sBase64 = base64_encode($sData);
		return strtr($sBase64, '+/', '-_');
	}
	
	private static function decode_base64($sData){
		$sBase64 = strtr($sData, '-_', '+/');
		return base64_decode($sBase64);
	}
	
}

?>