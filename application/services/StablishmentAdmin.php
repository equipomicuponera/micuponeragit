<?php

class Application_Service_StablishmentAdmin
{
    
    public function addStablishmentAdmin($stablishmentAdmin){
        
        $jsonData = json_encode($stablishmentAdmin);
        	
        $userService = new Application_Service_User();
        $newUser = $userService->addUser($stablishmentAdmin->getUser());
        $idUser = $newUser->id;
        
         $restService = new Application_Service_REST();
         $response = $restService->callWebService($this->url."stablishmentAdmin/".$idUser, $jsonData, 'POST');
         $newStablishmentAdmin = json_decode($response);
         return $newStablishmentAdmin;
        
    }
    
    public function updateStablishmentAdmin($stablishmentAdmin){
    
    	$jsonData = json_encode($stablishmentAdmin);
    	 
    	$restService = new Application_Service_REST();
    	$response = $restService->callWebService($this->url."stablishmentAdmin/".$stablishmentAdmin->getId(), $jsonData, 'PUT');
    	$updatedStablishmentAdmin = json_decode($response);
    	return $updatedStablishmentAdmin;
    
    }
    
    public function deleteStablishmentAdmin($stablishmentAdmin){
    
    	$jsonData = json_encode($stablishmentAdmin);
    
    	$restService = new Application_Service_REST();
    	$response = $restService->callWebService($this->url."stablishmentAdmin/".$stablishmentAdmin->getId(), $jsonData, 'DELETE');
    	$deletedStablishmentAdmin = json_decode($response);
    	return $deletedStablishmentAdmin;
    
    }
    
    public function getAllStablishmentAdmin(){
        $stablishmentAdminArray = array();
        $restService = new Application_Service_REST();
        $response = $restService->callWebService($this->url."stablishmentAdmin", null, 'GET');
        	$stablishmentAdminList = json_decode($response);
        	foreach ($stablishmentAdminList as $stablishmentAdminElement){
        	    $stablishmentAdmin = new Application_Model_StablishmentAdmin();
        	    $stablishmentAdmin->setId($stablishmentAdminElement->id);
        	    $stablishmentAdmin->setName($stablishmentAdminElement->name);
        	    $stablishmentAdmin->setLastName($stablishmentAdminElement->last_name);
        	    $stablishmentAdmin->setEmail($stablishmentAdminElement->email);
        	    $stablishmentAdmin->setStatus($stablishmentAdminElement->status);
        	    
        	    $userService = new Application_Service_User();
        	    $user = $userService->getOneUserById($stablishmentAdminElement->user_id);
        	    
        	    $stablishmentAdmin->setUser($user);
        	    
        	    array_push($stablishmentAdminArray, $stablishmentAdmin);
        	}
        return $stablishmentAdminArray;
    }
    
    public function getOneStablishmentAdminById($idStablishmentAdmin){
        $restService = new Application_Service_REST();
        $response = $restService->callWebService($this->url."stablishmentAdmin/".$idStablishmentAdmin, null, 'GET');
        $stablishmentAdminElement = json_decode($response);
                $stablishmentAdmin = new Application_Model_StablishmentAdmin();
        	    $stablishmentAdmin->setId($stablishmentAdminElement->id);
        	    $stablishmentAdmin->setName($stablishmentAdminElement->name);
        	    $stablishmentAdmin->setLastName($stablishmentAdminElement->last_name);
        	    $stablishmentAdmin->setEmail($stablishmentAdminElement->email);
        	    $stablishmentAdmin->setStatus($stablishmentAdminElement->status);
        	    
        	    $userService = new Application_Service_User();
        	    $user = $userService->getOneUserById($stablishmentAdminElement->user_id);
        	    
        	    $stablishmentAdmin->setUser($user);
        return $stablishmentAdmin;
    }
    
    public function getOneStablishmentAdminByUserId($idUser){
    	$restService = new Application_Service_REST();
    	$response = $restService->callWebService($this->url."stablishmentAdminByUserId/".$idUser, null, 'GET');
    	$stablishmentAdminElement = json_decode($response);
    	$stablishmentAdmin = new Application_Model_StablishmentAdmin();
    	$stablishmentAdmin->setId($stablishmentAdminElement->id);
    	$stablishmentAdmin->setName($stablishmentAdminElement->name);
    	$stablishmentAdmin->setLastName($stablishmentAdminElement->last_name);
    	$stablishmentAdmin->setEmail($stablishmentAdminElement->email);
    	$stablishmentAdmin->setStatus($stablishmentAdminElement->status);
    	 
    	$userService = new Application_Service_User();
    	$user = $userService->getOneUserById($stablishmentAdminElement->user_id);
    	 
    	$stablishmentAdmin->setUser($user);
    	return $stablishmentAdmin;
    }
    
    public function existEmail($email){
    	$restService = new Application_Service_REST();
    	$response = $restService->callWebService($this->url."validateEmail/".$email, null, 'GET');
    	$stablishmentAdminElement = json_decode($response);
    	 
    	if($stablishmentAdminElement!=false){//existe un usuario
    		$stablishmentAdmin = new Application_Model_StablishmentAdmin();
    	   $stablishmentAdmin->setId($stablishmentAdminElement->id);
        	$stablishmentAdmin->setName($stablishmentAdminElement->name);
        	$stablishmentAdmin->setLastName($stablishmentAdminElement->last_name);
        	$stablishmentAdmin->setEmail($stablishmentAdminElement->email);
        	$stablishmentAdmin->setStatus($stablishmentAdminElement->status);
        	 
        	$userService = new Application_Service_User();
        	$user = $userService->getOneUserById($stablishmentAdminElement->user_id);
        	$stablishmentAdmin->setUser($user);
        	return $stablishmentAdmin;
    	}else{//no existe
    		return null;
    	}
    }
    
    private $url = MAIN_URL_API;
}

?>