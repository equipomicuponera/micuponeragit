<?php

class RegisterController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
        $this->view->action = $this->getRequest()->getActionName();
    }
    
    public function indexAction(){
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getParams();
            $user = new Application_Model_User();
            $user->createFromArray($data);
            $user->setPassword($data["password"]);
            
            $userTypeService = new Application_Service_UserType();
            $userType = $userTypeService->getOneUserTypeById(Application_Model_UserType::$USER_TYPE_ARRAY["ADMIN"]);
            
            $user->setUserType($userType);
            
            $stablishmentAdmin = new Application_Model_StablishmentAdmin();
            $stablishmentAdmin->createFromArray($data);
            $stablishmentAdmin->setUser($user);
            $stablishmentAdmin->setStatus($this->ACTIVE);
            
            $stablishmentAdminService = new Application_Service_StablishmentAdmin();
            $stablishmentAdminService->addStablishmentAdmin($stablishmentAdmin);
        }
    }
    
    public function validateUsernameAction(){
    	if ($this->getRequest()->isPost()) {
    	    $this->_helper->layout()->disableLayout();
    	    $this->_helper->viewRenderer->setNoRender(true);
    		$data = $this->getRequest()->getParams();
    		$username = $data["username"];
    
    		$userService = new Application_Service_User();
    		$user = $userService->existUsername($username);
    		
    		if($user!=null){//existe
    		    echo $this->NOT_ACCEPTED;
    		}else{
    		    echo $this->ACCEPTED;
    		}
    	}
    }
    
    public function validateEmailAction(){
    	if ($this->getRequest()->isPost()) {
    	    $this->_helper->layout()->disableLayout();
    	    $this->_helper->viewRenderer->setNoRender(true);
    		$data = $this->getRequest()->getParams();
    		$email = $data["email"];
    
    		$stablishmentAdminService = new Application_Service_StablishmentAdmin();
    		$stablishmentAdmin = $stablishmentAdminService->existEmail($email);
    		if($stablishmentAdmin!=null){//existe
    			echo $this->NOT_ACCEPTED;
    		}else{
    			echo $this->ACCEPTED;
    		}
    	}
    }
    
    private $NOT_ACCEPTED = 1;
    private $ACCEPTED = 0;
    private $ACTIVE = 1;
}

