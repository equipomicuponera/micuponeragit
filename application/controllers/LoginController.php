<?php

class LoginController extends Zend_Controller_Action
{

    public function indexAction(){
        $this->_helper->layout()->disableLayout();
        if($this->getRequest()->isPost()){
        	$data = $this->getRequest()->getParams(); 
        	
        	$username = $data["userName"];
        	$password = $data["password"];
        	
        	$userService = new Application_Service_User();
        	$userElement = $userService->getUserByUsernameAndPassword($username, $password);
        	if($userElement!=null){
        	    if($userElement->getUserType()->getId() == Application_Model_UserType::$USER_TYPE_ARRAY["ADMIN"]){
        	        $stablishmentAdminService = new Application_Service_StablishmentAdmin();
        	        $stablishmentAdminElement = $stablishmentAdminService->getOneStablishmentAdminByUserId($userElement->id);
        	    	if($stablishmentAdminElement->getStatus()){
        	    	    $session = new Zend_Session_Namespace('cuponera');
        	    	    $session->stablishmentAdmin = $stablishmentAdminElement;
        	    		$this->_redirect('admin/stablishment');
        	    	}else{
        	    		$this->view->error = "Su cuenta ha sido desactivada";
					
        	    	}
        	    	 
        	    }else if($userElement->getUserType()->getId() == Application_Model_UserType::$USER_TYPE_ARRAY["SUPERADMIN"]){
        	        $session = new Zend_Session_Namespace('cuponera');
        	        $session->superadmin = $userElement;
        	    	$this->_redirect('superadmin/stablishment-admin');
        	    }else{
        	        $this->view->error = "Nombre de usuario o contrase&ntilde;a incorrectos";
        	    }
        	}else{
        	    $this->view->error = "Nombre de usuario o contrase&ntilde;a incorrectos";
        	}
        	
        }
    }
    
    public function logoutAction(){
        	Zend_Session::namespaceUnset('cuponera');
        	$this->_redirect('login');
    }
}
